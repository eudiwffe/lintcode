/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/array-partition-i
   @Language: C++
   @Datetime: 19-05-08 17:09
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @return: the sum of min(ai, bi) for all i from 1 to n
	 */
	int arrayPairSum(vector<int> &nums) {
		// Write your code here
		sort(nums.begin(),nums.end(),less<int>());
		int sum=0;
		for(int i=0; i<nums.size(); i+=2)
			sum+=nums[i];
		return sum;
	}
};
