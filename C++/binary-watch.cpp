/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-watch
   @Language: C++
   @Datetime: 19-04-17 10:51
   */

class Solution {
	int countone(int dec){
		int c=0;
		for(int i=1; i<=dec; i<<=1)
			c+=(i&dec?1:0);
		return c;
	}
public:
	/**
	 * @param num: the number of "1"s on a given timetable
	 * @return: all possible time
	 */
	vector<string> binaryTime(int num) {
		// Write your code here
		vector<string> vs;
		char str[8];
		for(int h=0; h<12; ++h)
			for(int m=0; m<60; ++m)
				if(countone(h)+countone(m)==num){
					sprintf(str,"%d:%02d",h,m);
					vs.push_back(str);
				}
		return vs;
	}
};
