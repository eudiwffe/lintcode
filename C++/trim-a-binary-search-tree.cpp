/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/trim-a-binary-search-tree
   @Language: C++
   @Datetime: 19-03-29 15:12
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: given BST
	 * @param minimum: the lower limit
	 * @param maximum: the upper limit
	 * @return: the root of the new tree 
	 */
	TreeNode * trimBST(TreeNode * root, int minimum, int maximum) {
		// write your code here
		if (root==NULL) return NULL;
		if (root->val<minimum) return trimBST(root->right, minimum, maximum);
		if (root->val>maximum) return trimBST(root->left, minimum, maximum);
		root->left=trimBST(root->left,minimum,maximum);
		root->right=trimBST(root->right,minimum,maximum);
		return root;
	}
};
