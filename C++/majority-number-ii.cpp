/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/majority-number-ii
   @Language: C++
   @Datetime: 16-02-09 05:59
   */

class Solution {
public:
	/**
	 * @param nums: A list of integers
	 * @return: The majority number occurs more than 1/3.
	 */
	/** Tips : see problem 48 : Majority Number iii
	 * */
	int majorityNumber(vector<int> nums) {
		// write your code here
		int k = 3;
		unordered_map<int,int> cans;
		unordered_map<int,int>::iterator it;
		for(int i=nums.size(); i--;){
			++cans[nums[i]];
			if (cans.size() < k) continue;
			// erase k elements each time
			for (it=cans.begin(); it!=cans.end();){
				if (--(it->second)) ++it;
				else cans.erase(it++);
			}
		}
		// reset count for candidates
		for(it=cans.begin(); it!=cans.end(); (it++)->second=0);
		// count each candidate elements occur times
		for(int i=nums.size(); i--;){
			it = cans.find(nums[i]);
			if (it != cans.end()) ++(it->second);
		}
		// find candidate element who occurs > n/k times
		for(it=cans.begin(); it!=cans.end(); ++it)
			if (it->second > 1.0*nums.size()/k) return it->first;
		return -1;
	}
};
