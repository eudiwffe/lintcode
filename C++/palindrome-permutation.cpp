/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/palindrome-permutation
   @Language: C++
   @Datetime: 19-05-07 14:50
   */

class Solution {
public:
	/**
	 * @param s: the given string
	 * @return: if a permutation of the string could form a palindrome
	 */
	bool canPermutePalindrome(string &s) {
		// write your code here
		int hash[128]={0}, count=0;
		for(int i=s.length(); i--; hash[s[i]]++);
		for(int i=128; i-- && count<2; count+=hash[i]&1);
		return count<2;
	}
};
