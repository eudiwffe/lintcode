/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/count-binary-substring
   @Language: C++
   @Datetime: 19-05-13 17:02
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: the number of substrings
	 */
	int countBinarySubstrings(string &s) {
		// Write your code here
		int one=0, zero=0, res=0;
		for(int i=0; i<s.length(); ++i){
			if(i>0 && s[i-1]!=s[i]) {
				++res;
				if(s[i]=='0') zero=1;
				if(s[i]=='1') one=1;
				continue;
			}
			if(s[i]=='0'){
				if(one>zero) ++res;
				++zero;
			}
			else if(s[i]=='1'){
				if(zero>one) ++res;
				++one;
			}
		}
		return res;
	}
};
