/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/isomorphic-strings
   @Language: C++
   @Datetime: 19-04-24 11:53
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @param t: a string
	 * @return: true if the characters in s can be replaced to get t or false
	 */
	bool isIsomorphic(string &s, string &t) {
		// write your code here
		if(s.length()!=t.length()) return false;
		unordered_map<char,char> dict;
		unordered_set<char> visit;
		for(int i=0; i<s.length(); ++i){
			if(dict.count(s[i])){
				if(dict[s[i]]!=t[i]) return false;
				continue;
			}
			if(visit.count(t[i])) return false;
			dict[s[i]]=t[i];
			visit.insert(t[i]);
		}
		return true;
	}
};
