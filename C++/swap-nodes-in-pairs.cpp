/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/swap-nodes-in-pairs
   @Language: C++
   @Datetime: 16-02-09 05:19
   */

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
	/**
	 * @param head a ListNode
	 * @return a ListNode
	 */
	ListNode* swapPairs(ListNode* head) {
		// Write your code here
		if (!head || !head->next) return head;
		ListNode H(-1), *l, *r;
		for(l=head, head=&H; l && (r=l->next); l=l->next){
			l->next = r->next;
			r->next = l;
			head->next = r;
			head = l;
		}
		return H.next;
	}
};
