/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/subdomain-visit-count
   @Language: C++
   @Datetime: 19-05-09 10:11
   */

class Solution {
public:
	/**
	 * @param cpdomains: a list cpdomains of count-paired domains
	 * @return: a list of count-paired domains
	 */
	vector<string> subdomainVisits(vector<string> &cpdomains) {
		// Write your code here
		unordered_map<string,int> dict;
		char str[128];
		for(int i=cpdomains.size(), count=0; i--;){
			sscanf(cpdomains[i].c_str(),"%d %s",&count,str);
			dict[str]+=count;
			for(char *pos=str; *pos!='\0';){
				pos = strchr(pos,'.');
				if(pos==NULL || *pos=='\0') break;
				dict[++pos]+=count;
			}
		}
		vector<string> vs;
		for(const auto &p:dict){
			sprintf(str,"%d %s",p.second,p.first.c_str());
			vs.push_back(str);
		}
		return vs;
	}
};
