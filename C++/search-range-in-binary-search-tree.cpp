/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/search-range-in-binary-search-tree
   @Language: C++
   @Datetime: 16-02-09 08:28
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */
class Solution {
	void searchRangeCore(TreeNode *rt,int k1,int k2,vector<int> &vs){
		if (rt==NULL) return;
		if (rt->val>=k1) searchRangeCore(rt->left,k1,k2,vs);
		if (rt->val>=k1 && rt->val<=k2) vs.push_back(rt->val);
		if (rt->val<=k2) searchRangeCore(rt->right,k1,k2,vs);
	}

public:
	/**
	 * @param root: The root of the binary search tree.
	 * @param k1 and k2: range k1 to k2.
	 * @return: Return all keys that k1<=key<=k2 in ascending order.
	 */
	vector<int> searchRange(TreeNode* root, int k1, int k2) {
		// write your code here
		vector<int> vs;
		searchRangeCore(root,k1,k2,vs);
		return vs;
	}
};
