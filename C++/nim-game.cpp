/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/nim-game
   @Language: C++
   @Datetime: 19-05-27 10:08
   */

class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: whether you can win the game given the number of stones in the heap
	 */
	bool canWinNim(int n) {
		// Write your code here
		return n%4;
	}
};
