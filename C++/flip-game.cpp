/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/flip-game
   @Language: C++
   @Datetime: 19-05-07 14:38
   */

class Solution {
public:
	/**
	 * @param s: the given string
	 * @return: all the possible states of the string after one valid move
	 */
	vector<string> generatePossibleNextMoves(string &s) {
		// write your code here
		vector<string> vs;
		for(int i=1; i<s.length(); ++i){
			if(s[i-1]=='+' && s[i]=='+'){
				s[i-1]=s[i]='-';
				vs.push_back(s);
				s[i-1]=s[i]='+';
			}
		}
		return vs;
	}
};
