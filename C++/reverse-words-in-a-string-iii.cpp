/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-words-in-a-string-iii
   @Language: C++
   @Datetime: 19-05-15 17:42
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order
	 */
	string reverseWords(string &s) {
		// Write your code here
		for(int start=0, end=0; start<s.length(); start=end){
			start=s.find_first_not_of(' ',start);
			if(start==s.npos) break;
			end=s.find(' ',start);
			if(end==s.npos) end=s.length();
			reverse(s.begin()+start,s.begin()+end);
		}
		return s;
	}
};
