/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/implement-queue-by-two-stacks
   @Language: C++
   @Datetime: 16-02-09 05:53
   */

class MyQueue {
public:
	stack<int> stack1;
	stack<int> stack2;

	MyQueue() {
		// do intialization if necessary
	}

	void push(int element) {
		// write your code here
		stack1.push(element);
	}

	int pop() {
		// write your code here
		int ele = top();
		stack2.pop();
		return ele;
	}

	int top() {
		// write your code here
		if (stack2.size()) return stack2.top();
		for(; stack1.size(); stack1.pop())
			stack2.push(stack1.top());
		return (stack2.size()>0 ? stack2.top() : 0);
	}
};
