/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/one-edit-distance
   @Language: C++
   @Datetime: 19-06-11 16:57
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @param t: a string
	 * @return: true if they are both one edit distance apart or false
	 */
	bool isOneEditDistance(string &s, string &t) {
		// write your code here
		const int dist=abs((int)s.length()-(int)t.length());
		if(dist>1) return false;
		int hash[128]={0};
		for(int i=s.length(); i--; ++hash[s[i]]);
		for(int i=t.length(); i--; --hash[t[i]]);
		int diff=0;
		for(int i=0; i<128; diff+=hash[i++]!=0);
		if(dist==1 && diff==1) return true;
		if(dist==0 && diff==2) return true;
		return false;
	}
};
