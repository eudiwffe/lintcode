/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/check-full-binary-tree
   @Language: C++
   @Datetime: 19-06-25 09:43
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: the given tree
	 * @return: Whether it is a full tree
	 */
	bool isFullTree(TreeNode * root) {
		// write your code here
		if(root==NULL) return true;
		if(root->left && root->right) return isFullTree(root->left) && isFullTree(root->right);
		return root->left==NULL && root->right==NULL;
	}
};
