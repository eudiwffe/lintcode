/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/capitalizes-the-first-letter
   @Language: C++
   @Datetime: 19-05-07 16:32
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: a string after capitalizes the first letter
	 */
	string capitalizesFirst(string &s) {
		// Write your code here
		for(int i=0; i<s.length(); i=s.find(' ',i)){
			i = s.find_first_not_of(' ',i);
			if(i<0 || i>=s.length()) break;
			s[i]=s[i]-'a'+'A';
		}
		return s;
	}
};
