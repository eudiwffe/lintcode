/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/swap-two-integers-in-array
   @Language: C++
   @Datetime: 19-03-31 15:19
   */

class Solution {
public:
	/**
	 * @param A: An integer array
	 * @param index1: the first index
	 * @param index2: the second index
	 * @return: nothing
	 */
	void swapIntegers(vector<int> &A, int index1, int index2) {
		// write your code here
		swap(A[index1],A[index2]);
	}
};
