/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/o1-check-power-of-2
   @Language: C++
   @Datetime: 16-02-09 05:13
   */

class Solution {
public:
	/*
	 * @param n: An integer
	 * @return: True or false
	 */
	bool checkPowerOf2(int n) {
		// write your code here
		return (n>0 ? !(n&(n-1)) : false);
	}
};
