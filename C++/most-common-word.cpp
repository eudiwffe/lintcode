/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/most-frequent-subtree-sum
   @Language: C++
   @Datetime: 19-05-15 17:06
   */

class Solution {
	static bool issplit(const char &ch){
		const string split=" !?',;.";
		return split.find(ch)!=split.npos;
	}
public:
	/**
	 * @param paragraph: 
	 * @param banned: 
	 * @return: nothing
	 */
	string mostCommonWord(string &paragraph, vector<string> &banned) {
		// 
		unordered_set<string> bans;
		for(int i=banned.size(); i--;){
			transform(banned[i].begin(),banned[i].end(),banned[i].begin(),::tolower);
			bans.insert(banned[i]);
		}
		unordered_map<string,int> freqs;
		int count=0;
		string res, word;
		replace_if(paragraph.begin(),paragraph.end(),issplit,' ');
		for(stringstream istr(paragraph); istr>>word;){
			transform(word.begin(),word.end(),word.begin(),::tolower);
			if(bans.count(word)) continue;
			freqs[word]++;
			if(freqs[word]>count){
				count=freqs[word];
				res=word;
			}
		}

		return res;
	}
};
