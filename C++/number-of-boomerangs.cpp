/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/number-of-boomerangs
   @Language: C++
   @Datetime: 19-05-22 15:44
   */


class Solution {
public:
	/**
	 * @param points: a 2D array
	 * @return: the number of boomerangs
	 */
	int numberOfBoomerangs(vector<vector<int> > &points) {
		// Write your code here
		int count=0;
		for(int i=0; i<points.size(); ++i){
			unordered_map<int,int> dists;
			for(int j=0; j<points.size(); ++j){
				const int dist=(points[i][0]-points[j][0])*(points[i][0]-points[j][0])
					+(points[i][1]-points[j][1])*(points[i][1]-points[j][1]);
				dists[dist]++;
			}
			for(const auto d:dists)
				count+=d.second*(d.second-1);
		}
		return count;
	}
};
