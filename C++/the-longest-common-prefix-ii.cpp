/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/the-longest-common-prefix-ii
   @Language: C++
   @Datetime: 19-04-27 09:45
   */

class Solution {
public:
	/**
	 * @param dic: the n strings
	 * @param target: the target string
	 * @return: The ans
	 */
	int the_longest_common_prefix(vector<string> &dic, string &target) {
		// write your code here
		int prefix=0;
		for(const string &s:dic){
			for(int i=0; i<s.length() && i<target.length(); ++i)
				if(s[i]!=target[i]) break;
				else prefix=max(prefix,i+1);
		}
		return prefix;
	}
};
