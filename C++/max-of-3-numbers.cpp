/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/max-of-3-numbers
   @Language: C++
   @Datetime: 20-01-10 10:14
   */

class Solution {
public:
	/**
	 * @param num1: An integer
	 * @param num2: An integer
	 * @param num3: An integer
	 * @return: an interger
	 */
	int maxOfThreeNumbers(int num1, int num2, int num3) {
		// write your code here
		return max(num1, max(num2, num3));
	}
};
