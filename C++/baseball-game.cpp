/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/baseball-game
   @Language: C++
   @Datetime: 19-05-08 14:38
   */

class Solution {
public:
	/**
	 * @param ops: the list of operations
	 * @return:  the sum of the points you could get in all the rounds
	 */
	int calPoints(vector<string> &ops) {
		// Write your code here
		vector<int> v;
		for(const string &str:ops){
			switch(str[0]){
				case '+':v.push_back(v[v.size()-1]+v[v.size()-2]); break;
				case 'C':v.pop_back(); break;
				case 'D':v.push_back(v.back()*2); break;
				default :v.push_back(atoi(str.c_str())); break;
			}
		}
		int sum=0;
		for(int i=v.size(); i--; sum+=v[i]);
		return sum;
	}
};
