/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/construct-string-from-binary-tree
   @Language: C++
   @Datetime: 19-05-14 16:45
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param t: the root of tree
	 * @return: return a string
	 */
	string tree2str(TreeNode * t) {
		// write your code here
		if(t==NULL) return "";
		string str=to_string(t->val);
		if(t->left==NULL && t->right) str.append("()");
		if(t->left) str.append("("+tree2str(t->left)+")");
		if(t->right) str.append("("+tree2str(t->right)+")");
		return str;
	}
};
