/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/validate-binary-search-tree
   @Language: C++
   @Datetime: 19-03-29 13:30
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	TreeNode *last=NULL;
public:
	/**
	 * @param root: The root of binary tree.
	 * @return: True if the binary tree is BST, or false
	 */
	bool isValidBST(TreeNode * root) {
		// write your code here
		if (root==NULL) return true;
		if (!isValidBST(root->left)) return false;
		if (last && last!=root && last->val>=root->val) return false;
		last = root;
		if (!isValidBST(root->right)) return false;
		return true;
	}
};
