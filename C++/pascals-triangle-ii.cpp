/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/pascals-triangle-ii
   @Language: C++
   @Datetime: 19-04-22 10:22
   */

class Solution {
public:
	/**
	 * @param rowIndex: a non-negative index
	 * @return: the kth index row of the Pascal's triangle
	 */
	vector<int> getRow(int rowIndex) {
		// write your code here
		vector<int> v(rowIndex+1,1);
		for(int f=rowIndex, i=1; i<=rowIndex/2; ++i){
			v[i]=v[rowIndex-i]=f;
			f=(long)f*(rowIndex-i)/(i+1);
		}
		return v;
	}
};
