/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/base-7
   @Language: C++
   @Datetime: 19-05-08 14:18
   */

class Solution {
public:
	/**
	 * @param num: the given number
	 * @return: The base 7 string representation
	 */
	string convertToBase7(int num) {
		// Write your code here
		if(num<0) return convertToBase7(-num).insert(0,"-");
		string s;
		for(; num; num/=7)
			s.push_back('0'+num%7);
		reverse(s.begin(),s.end());
		return s;
	}
};
