/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/single-number
   @Language: C++
   @Datetime: 16-02-09 05:17
   */

class Solution {
public:
	/**
	 * @param A: Array of integers.
	 * return: The single number.
	 * Tip: a^a equal to 0.
	 */
	int singleNumber(vector<int> &A) {
		int a=0;
		for(int i=A.size(); i; a^=A[--i]);
		return a;
	}
};
