/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/prime-number-of-set-bits-in-binary-representation
   @Language: C++
   @Datetime: 19-05-13 13:42
   */

class Solution {
public:
	/**
	 * @param L: an integer
	 * @param R: an integer
	 * @return: the count of numbers in the range [L, R] having a prime number of set bits in their binary representation
	 * Tip: L, R in range [1,10^6], binary bits must <=20
	 */
	int countPrimeSetBits(int L, int R) {
		// Write your code here
		int count=0;
		bool prime[]={0,0,1,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0};
		for(int i=L; i<=R; ++i){
			int bit=0;
			for(int a=i; a; a=a&(a-1))
				++bit;
			if(prime[bit]) ++count;
		}
		return count;
	}
};
