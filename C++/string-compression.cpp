/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/string-compression
   @Language: C++
   @Datetime: 19-03-17 10:40
   */

class Solution {
public:
	/**
	 * @param originalString: a string
	 * @return: a compressed string
	 */
	string compress(string &org) {
		// write your code here
		int count=1;
		string res;
		char str[32];
		for(int i=1; i<org.length(); ++i){
			if(org[i]==org[i-1]) ++count;
			else{
				sprintf(str,"%c%d",org[i-1],count);
				res.append(str);
				count=1;
			}
		}
		sprintf(str,"%c%d",org.back(),count);
		res.append(str);
		return res.length()<org.length()?res:org;
	}
};
