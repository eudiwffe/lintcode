/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-level-order-traversal-ii
   @Language: C++
   @Datetime: 16-02-09 05:43
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	/**
	 * @param root : The root of binary tree.
	 * @return : buttom-up level order a list of lists of integer
	 */
public:
	vector<vector<int> > levelOrderBottom(TreeNode *root) {
		// write your code here
		vector<vector<int> > vs;
		if (root==NULL) return vs;
		queue<TreeNode*> que;
		for(que.push(root); que.size();){
			int size = que.size();
			vector<int> v(size);
			for(int i=0; i<size; ++i, que.pop()){
				TreeNode *q = que.front();
				v[i] = q->val;
				if(q->left) que.push(q->left);
				if(q->right) que.push(q->right);
			}
			vs.insert(vs.begin(),v);
		}
		return vs;
	}
};
