/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/linked-list-cycle
   @Language: C++
   @Datetime: 16-02-09 05:59
   */

/**
 * Definition of ListNode
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *         this->val = val;
 *         this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param head: The first node of linked list.
	 * @return: True if it has a cycle, or false
	 */
	bool hasCycle(ListNode *head) {
		// write your code here
		for(ListNode *p=head; p && (p=p->next); head=head->next, p=p->next)
			if (p==head) return true;
		return false;
	}
};
