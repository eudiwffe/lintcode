/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/convert-sorted-array-to-binary-search-tree-with-minimal-height
   @Language: C++
   @Datetime: 16-02-09 04:48
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	TreeNode *transbst(vector<int> &A,int start,int end){
		if (start>end)
			return NULL;
		int mid = (start+end)/2;
		TreeNode * r = new TreeNode(A[mid]);
		r->left = transbst(A,start,mid-1);
		r->right = transbst(A,mid+1,end);
		return r;
	}
public:
	/**
	 * @param A: A sorted (increasing order) array
	 * @return: A tree node
	 */
	TreeNode *sortedArrayToBST(vector<int> &A) {
		// write your code here
		return transbst(A,0,A.size()-1);
	}
};

