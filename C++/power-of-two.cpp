/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/power-of-two
   @Language: C++
   @Datetime: 19-05-23 13:55
   */

// Method 1, log and power, 50ms
class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: if n is a power of two
	 */
	bool isPowerOfTwo(int n) {
		// Write your code here
		if(n==0) return false;
		return n==(int)pow(2,(int)log2(n));
	}
};

// Method 2, bit, Time 50ms
class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: if n is a power of two
	 */
	bool isPowerOfTwo(int n) {
		// Write your code here
		if(n<2) return n;
		return ((n-1)&(~n))==n-1;
	}
};
