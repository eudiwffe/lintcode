/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/container-with-most-water
   @Language: C++
   @Datetime: 16-02-09 05:46
   */

class Solution {
public:
	/**
	 * @param heights: a vector of integers
	 * @return: an integer
	 */
	int maxArea(vector<int> &H) {
		// write your code here
		int s=0;
		for (int i=0, j=H.size()-1; i<j;){
			s = max(s,(j-i)*min(H[i],H[j]));
			H[i]<H[j] ? ++i : --j;
		}
		return s;
	}
};
