/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-word-square
   @Language: C++
   @Datetime: 19-05-06 14:40
   */

class Solution {
public:
	/**
	 * @param words: a list of string
	 * @return: a boolean
	 */
	bool validWordSquare(vector<string> &words) {
		// Write your code here
		for(int row=0, rows=words.size(); row<rows; ++row){
			int i=0, cols=words[row].size();
			for(; i<cols && i<rows && row<words[i].size(); ++i)
				if(words[row][i]!=words[i][row]) return false;
			if(i!=cols) return false;
		}
		return true;
	}
};
