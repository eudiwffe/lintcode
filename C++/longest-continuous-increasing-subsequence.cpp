/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-increasing-continuous-subsequence
   @Language: C++
   @Datetime: 17-04-01 22:30
   */

class Solution {
public:
	/**
	 * @param A an array of Integer
	 * @return  an integer
	 */
	int longestIncreasingContinuousSubsequence(vector<int>& A) {
		// Write your code here
		int incmax=0, inc=0, decmax=0, dec=0;
		for(int i=0; i<A.size(); ++i){
			if (i==0 || A[i]==A[i-1]){
				incmax = max(incmax,++inc);
				decmax = max(decmax,++dec);
			}
			else if (A[i]>A[i-1]){
				incmax = max(incmax,++inc);
				dec = 1;
			}
			else if (A[i]<A[i-1]){
				decmax = max(decmax,++dec);
				inc = 1;
			}
		}
		return max(incmax,decmax);
	}
};
