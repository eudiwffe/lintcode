/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/count-linked-list-nodes
   @Language: C++
   @Datetime: 19-03-31 10:47
   */

/**
 * Definition of singly-linked-list:
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *        this->val = val;
 *        this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param head: the first node of linked list.
	 * @return: An integer
	 */
	int countNodes(ListNode * head) {
		// write your code here
		int n=0;
		for(;head; ++n, head=head->next);
		return n;
	}
};
