/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/ugly-number
   @Language: C++
   @Datetime: 16-12-07 10:01
   */

class Solution {
public:
	/**
	 * @param num an integer
	 * @return true if num is an ugly number or false
	 */
	bool isUgly(int num) {
		// Write your code here
		for(;num && num%2==0;num/=2);
		for(;num && num%3==0;num/=3);
		for(;num && num%5==0;num/=5);
		return num==1;
	}
};
