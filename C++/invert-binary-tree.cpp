/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/invert-binary-tree
   @Language: C++
   @Datetime: 16-02-09 04:50
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: a TreeNode, the root of the binary tree
	 * @return: nothing
	 */
	void invertBinaryTree(TreeNode * root) {
		// write your code here
		if (root==NULL) return;
		swap(root->left,root->right);
		invertBinaryTree(root->left);
		invertBinaryTree(root->right);
	}
};
