/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/set-mismatch
   @Language: C++
   @Datetime: 19-05-14 15:36
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @return: the number occurs twice and the number that is missing
	 */
	vector<int> findErrorNums(vector<int> &nums) {
		// Write your code here
		vector<int> bits(nums.size()+1,0);
		int dup, lose;
		for(int i=0; i<nums.size(); ++i){
			bits[nums[i]]++;
			if(bits[nums[i]]>1) dup=nums[i];
		}
		for(int i=1; i<bits.size(); ++i){
			if(bits[i]==0){
				lose=i;
				break;
			}
		}
		return {dup,lose};
	}
};
