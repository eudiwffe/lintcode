/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reorder-array-to-construct-the-minimum-number
   @Language: C++
   @Datetime: 16-12-09 03:27
   */

class Solution {
	struct comp{
		bool operator()(const int a,const int b)const{
			char sa[32], sb[32];
			sprintf(sa,"%d%d",a,b);
			sprintf(sb,"%d%d",b,a);
			return strcmp(sa,sb)<0;
		}
	};
public:
	/**
	 * @param nums n non-negative integer array
	 * @return a string
	 */
	string minNumber(vector<int>& nums) {
		// Write your code here
		sort(nums.begin(),nums.end(),comp());
		string s;
		char str[16];
		for(int i=0;i<nums.size();++i){
			if (nums[i]==0) continue;
			sprintf(str,"%d",nums[i]);
			s.append(str);
		}
		return (s.length()?s:"0");
	}
};
