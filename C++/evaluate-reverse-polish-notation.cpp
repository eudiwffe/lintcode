/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/evaluate-reverse-polish-notation
   @Language: C++
   @Datetime: 16-02-09 05:48
   */

class Solution {
public:
	/**
	 * @param tokens: The Reverse Polish Notation
	 * @return: the value
	 */
	int evalRPN(vector<string> &tokens) {
		// write your code here
		stack<int> s;
		for(const auto &t:tokens){
			if(isdigit(t[0]) || t.length()>1) {
				s.push(atoi(t.c_str()));
				continue;
			}
			int b=s.top(); s.pop();
			int a=s.top(); s.pop();
			switch(t[0]){
				case '+' : s.push(a+b); break;
				case '-' : s.push(a-b); break;
				case '*' : s.push(a*b); break;
				case '/' : s.push(a/b); break;
				default : break;
			}
		}
		return s.top();
	}
};
