/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/add-digits
   @Language: C++
   @Datetime: 17-04-01 21:51
   */

class Solution {
public:
	/**
	 * @param num a non-negative integer
	 * @return one digit
	 */
	int addDigits(int num) {
		// Write your code here
		if (num==0) return 0;
		return (num%9 ? num%9 : 9);
	}
};
