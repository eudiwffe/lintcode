/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/first-unique-number-in-data-stream
   @Language: C++
   @Datetime: 19-03-17 16:52
   */

class Solution {
public:
	/**
	 * @param nums: a continuous stream of numbers
	 * @param number: a number
	 * @return: returns the first unique number
	 */
	int firstUniqueNumber(vector<int> &nums, int number) {
		// Write your code here
		unordered_map<int,int> dict;    // num, index
		map<int,int> unqiue;            // index, num
		int i=0;
		for(; i<nums.size() && nums[i]!=number; i++){
			unordered_map<int,int>::iterator it=dict.find(nums[i]);
			if (it==dict.end()){
				dict[nums[i]]=i;
				unqiue[i]=nums[i];
			}
			else unqiue.erase(it->second);
		}
		if (i<nums.size()){
			if (dict.find(number)==dict.end())
				unqiue[i]=number;
		}
		if(unqiue.size()<1 || i==nums.size()) return -1;
		else return unqiue.begin()->second;
	}
};
