/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-absolute-difference-in-bst
   @Language: C++
   @Datetime: 19-05-16 09:42
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	void inorder(TreeNode *root, vector<int> &v){
		if(root==NULL) return;
		inorder(root->left,v);
		v.push_back(root->val);
		inorder(root->right,v);
	}
public:
	/**
	 * @param root: the root
	 * @return: the minimum absolute difference between values of any two nodes
	 */
	int getMinimumDifference(TreeNode * root) {
		// Write your code here
		vector<int> v;
		inorder(root,v);
		int diff=INT_MAX;
		for(int i=1; i<v.size(); ++i)
			diff=min(diff,abs(v[i]-v[i-1]));
		return diff;
	}
};
