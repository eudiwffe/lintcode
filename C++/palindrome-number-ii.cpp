/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/palindrome-number-ii
   @Language: C++
   @Datetime: 19-04-25 17:09
   */

class Solution {
public:
	/**
	 * @param n: non-negative integer n.
	 * @return: return whether a binary representation of a non-negative integer n is a palindrome.
	 */
	bool isPalindrome(int n) {
		// Write your code here
		string str;
		for(; n; n>>=1)
			str.push_back(n&1);
		for(int i=0, j=str.length()-1; i<j;)
			if(str[i++]!=str[j--]) return false;
		return true;
	}
};
