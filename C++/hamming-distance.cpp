/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/hamming-distance
   @Language: C++
   @Datetime: 19-04-27 15:36
   */

class Solution {
public:
	/**
	 * @param x: an integer
	 * @param y: an integer
	 * @return: return an integer, denote the Hamming Distance between two integers
	 */
	int hammingDistance(int x, int y) {
		// write your code here
		int count =0;
		for(long i=1; i<=x || i<=y; i<<=1)
			if((i&x)!=(i&y)) ++count;
		return count;
	}
};
