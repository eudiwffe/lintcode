/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/range-addition
   @Language: C++
   @Datetime: 19-05-15 14:02
   */

class Solution {
public:
	/**
	 * @param length: the length of the array
	 * @param updates: update operations
	 * @return: the modified array after all k operations were executed
	 */
	vector<int> getModifiedArray(int length, vector<vector<int> > &updates) {
		// Write your code here
		vector<int> v(length+1,0);
		for(const auto &ops:updates){
			v[ops[0]]+=ops[2];
			v[ops[1]+1]-=ops[2];
		}
		for(int i=1; i<length; ++i)
			v[i]+=v[i-1];
		v.pop_back();
		return v;
	}
};
