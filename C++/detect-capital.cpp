/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/detect-capital
   @Language: C++
   @Datetime: 19-05-16 13:38
   */

class Solution {
public:
	/**
	 * @param word: a string
	 * @return: return a boolean
	 */
	bool detectCapitalUse(string &word) {
		// write your code here
		for(int i=0, id=0, cap=0; i<word.length(); ++i){
			if(isupper(word[i])) ++cap, id=i;
			if(id!=0 && cap!=i+1) return false;
		}
		return true;
	}
};
