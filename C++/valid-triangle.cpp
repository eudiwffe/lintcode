/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-triangle
   @Language: C++
   @Datetime: 19-04-19 17:10
   */

class Solution {
public:
	/**
	 * @param a: a integer represent the length of one edge
	 * @param b: a integer represent the length of one edge
	 * @param c: a integer represent the length of one edge
	 * @return: whether three edges can form a triangle
	 */
	bool isValidTriangle(int a, int b, int c) {
		// write your code here
		return (a+b>c && a+c>b && b+c>a);
	}
};
