/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/hex-conversion
   @Language: C++
   @Datetime: 19-03-28 17:16
   */

class Solution {
public:
	/**
	 * @param n: a decimal number
	 * @param k: a Integer represent base-k
	 * @return: a base-k number
	 */
	string hexConversion(int n, int k) {
		// write your code here
		if (n==0) return "0";
		string res;
		for(int r; n; n /= k){
			r = n%k;
			if (r>9) res.push_back(r-10+'A');
			else res.push_back(r+'0');
		}
		reverse(res.begin(), res.end());
		return res;
	}
};
