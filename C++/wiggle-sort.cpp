/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/wiggle-sort
   @Language: C++
   @Datetime: 19-04-03 14:06
   */

class Solution {
public:
	/*
	 * @param nums: A list of integers
	 * @return: nothing
	 */
	void wiggleSort(vector<int> &nums) {
		// write your code here
		for(int i=1; i<nums.size(); ++i)
			if((i&1 && nums[i]<nums[i-1])||(!(i&1) && nums[i]>nums[i-1]))
				swap(nums[i],nums[i-1]);
	}
};
