/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/assign-cookies
   @Language: C++
   @Datetime: 19-05-22 10:27
   */

class Solution {
public:
	/**
	 * @param g: children's greed factor
	 * @param s: cookie's size
	 * @return: the maximum number
	 */
	int findContentChildren(vector<int> &g, vector<int> &s) {
		// Write your code here
		sort(g.begin(),g.end());
		sort(s.begin(),s.end());
		int k=0;
		for(int i=0, j=0; i<g.size() && j<s.size();){
			if(s[j]>=g[i]) ++k, ++i, ++j;
			else ++j;
		}
		return k;
	}
};
