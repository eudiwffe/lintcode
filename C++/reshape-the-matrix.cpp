/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reshape-the-matrix
   @Language: C++
   @Datetime: 19-05-15 17:24
   */

class Solution {
public:
	/**
	 * @param nums: List[List[int]]
	 * @param r: an integer
	 * @param c: an integer
	 * @return: return List[List[int]]
	 */
	vector<vector<int> > matrixReshape(vector<vector<int> > &nums, int r, int c) {
		// write your code here
		int row=nums.size(), col=nums[0].size();
		if(row*col!=r*c) return nums;
		vector<vector<int> > res(r,vector<int>(c));
		r=c=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				res[r][c++]=nums[i][j];
				if(c>=res[0].size())
					++r, c=0;
			}
		}
		return res;
	}
};
