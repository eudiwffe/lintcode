/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-longest-consecutive-sequence
   @Language: C++
   @Datetime: 19-06-11 09:36
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	int maxdepth(TreeNode *root, int last, int sum, int mx){
		if(root==NULL) return mx;
		sum=root->val-last==1?sum+1:1;
		mx=max(mx,sum);
		return max(maxdepth(root->left,root->val,sum,mx),
				maxdepth(root->right,root->val,sum,mx));
	}
public:
	/**
	 * @param root: the root of binary tree
	 * @return: the length of the longest consecutive sequence path
	 */
	int longestConsecutive(TreeNode * root) {
		// write your code here
		return maxdepth(root,0,0,0);
	}
};
