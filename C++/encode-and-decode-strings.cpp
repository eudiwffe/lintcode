/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/encode-and-decode-strings
   @Language: C++
   @Datetime: 19-06-12 16:53
   */

class Solution {
public:
	/*
	 * @param strs: a list of strings
	 * @return: encodes a list of strings to a single string.
	 */
	string encode(vector<string> &strs) {
		// write your code here
		string res=to_string(strs.size());
		res.push_back('\0');
		for(const auto &str:strs){
			res.append(str);
			res.push_back('\0');
		}
		return res;
	}

	/*
	 * @param str: A string
	 * @return: dcodes a single string to a list of strings
	 */
	vector<string> decode(string &str) {
		// write your code here
		int cnt=atoi(str.c_str());
		vector<string> vs;
		for(int i=str.find('\0')+1, j=0; cnt--; i=j+1){
			j=str.find('\0',i);
			vs.push_back(str.substr(i,j-i));
		}
		return vs;
	}
};
