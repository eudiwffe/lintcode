/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/sort-letters-by-case
   @Language: C++
   @Datetime: 16-02-09 08:35
   */

class Solution {
public:
	/** 
	 * @param chars: The letters array you should sort.
	 */
	/* Tips: partition letters by case
	 * */
	void sortLetters(string &letters) {
		// write your code here
		for(int i=0, j=letters.length()-1; i<=j;){
			if (islower(letters[i])) ++i;
			else if (isupper(letters[j])) --j;
			else swap(letters[i++],letters[j--]);
		}
	}
};
