/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/insert-node-in-sorted-linked-list
   @Language: C++
   @Datetime: 19-03-14 17:04
   */

/**
 * Definition of singly-linked-list:
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *        this->val = val;
 *        this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param head: The head of linked list.
	 * @param val: An integer.
	 * @return: The head of new linked list.
	 */
	ListNode * insertNode(ListNode * head, int val) {
		// write your code here
		ListNode H(-1, head), *p;
		for(p=&H; p->next && p->next->val<val; p=p->next);
		p->next=new ListNode(val,p->next);
		return H.next;
	}
};
