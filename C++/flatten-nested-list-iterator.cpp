/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/flatten-nested-list-iterator
   @Language: C++
   @Datetime: 19-04-03 17:58
   */

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer,
 *     // rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds,
 *     // if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds,
 *     // if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */
class NestedIterator {
	stack<NestedInteger> mstack;
public:
	NestedIterator(vector<NestedInteger> &nestedList) {
		// Initialize your data structure here.
		for(int i=nestedList.size(); i--; mstack.push(nestedList[i]));
	}

	// @return {int} the next element in the iteration
	int next() {
		// Write your code here
		//if(hasNext()){
		NestedInteger ni=mstack.top();
		mstack.pop();
		return ni.getInteger();
		//}
		//return 0;
	}

	// @return {boolean} true if the iteration has more element or false
	bool hasNext() {
		// Write your code here
		for(; mstack.size();){
			NestedInteger ni=mstack.top();
			if(ni.isInteger()) return true;
			mstack.pop();
			const vector<NestedInteger> &nis=ni.getList();
			for(int i=nis.size(); i--; mstack.push(nis[i]));
		}
		return false;
	}
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) v.push_back(i.next());
 */
