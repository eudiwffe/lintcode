/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-harmonious-subsequence
   @Language: C++
   @Datetime: 19-05-15 13:17
   */

class Solution {
public:
	/**
	 * @param nums: a list of integers
	 * @return: return a integer
	 */
	int findLHS(vector<int> &nums) {
		// write your code here
		unordered_map<int,int> freq;
		for(int i=nums.size(); i--; ++freq[nums[i]]);
		int lhs=0;
		for(const auto &p:freq){
			if(freq.count(p.first+1)<1) continue;
			else lhs=max(lhs,p.second+freq[p.first+1]);
		}
		return lhs;
	}
};
