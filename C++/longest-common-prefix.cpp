/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-common-prefix
   @Language: C++
   @Datetime: 16-02-09 05:56
   */


class Solution {
public:
	/**
	 * @param strs: A list of strings
	 * @return: The longest common prefix
	 */
	string longestCommonPrefix(vector<string> &strs) {
		// write your code here
		string prefix;
		for(int i=0; strs.size() && i<strs[0].length(); ++i){
			const char ch=strs[0][i];
			for(const string &str:strs)
				if(i>=str.length() || str[i]!=ch) return prefix;
			prefix.push_back(ch);
		}
		return prefix;
	}
};

