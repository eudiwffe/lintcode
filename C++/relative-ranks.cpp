/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/relative-ranks
   @Language: C++
   @Datetime: 19-03-18 14:09
   */

class Solution {
public:
	/**
	 * @param nums: List[int]
	 * @return: return List[str]
	 */
	vector<string> findRelativeRanks(vector<int> &nums) {
		// write your code here
		map<int,int,greater<int> > scores;    // score, index
		for(int i=nums.size(); i--; scores[nums[i]]=i);
		vector<string> res(nums.size(),"0");
		int rank=1;
		string rankname[4]={"","Gold Medal","Silver Medal","Bronze Medal"};
		for(pair<int,int> p:scores){
			if (rank<4) res[p.second]=rankname[rank];
			else res[p.second]=to_string(rank);
			rank++;
		}
		return res;
	}
};
