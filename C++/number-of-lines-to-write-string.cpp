/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/number-of-lines-to-write-string
   @Language: C++
   @Datetime: 19-05-09 10:29
   */

class Solution {
public:
	/**
	 * @param widths: an array
	 * @param S: a string
	 * @return: how many lines have at least one character from S, and what is the width used by the last such line
	 */
	vector<int> numberOfLines(vector<int> &widths, string &S) {
		// Write your code here
		int row=1, col=0;
		for(const char &c:S){
			if(col+widths[c-'a']>100){
				++row;
				col=widths[c-'a'];
			}
			else col+=widths[c-'a'];
		}
		return {row,col};
	}
};
