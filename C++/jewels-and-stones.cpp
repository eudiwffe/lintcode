/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/jewels-and-stones
   @Language: C++
   @Datetime: 19-04-24 13:55
   */

class Solution {
public:
	/**
	 * @param J: the types of stones that are jewels
	 * @param S: representing the stones you have
	 * @return: how many of the stones you have are also jewels
	 */
	int numJewelsInStones(string &J, string &S) {
		// Write your code here
		unordered_set<char> jewels;
		int count=0;
		for(const char &c:J)
			jewels.insert(c);
		for(const char &c:S)
			count+=jewels.count(c);
		return count;
	}
};
