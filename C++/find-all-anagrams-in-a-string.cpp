/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-all-anagrams-in-a-string
   @Language: C++
   @Datetime: 19-04-24 15:49
   */

class Solution {
	bool ismatch(unordered_map<char,int> &match, unordered_map<char,int> &dict){
		if(match.size()!=dict.size()) return false;
		for(auto i=match.begin(); i!=match.end(); ++i)
			if(dict.count(i->first)<1 || dict[i->first]!=i->second) return false;
		return true;
	}
public:
	/**
	 * @param s: a string
	 * @param p: a string
	 * @return: a list of index
	 */
	vector<int> findAnagrams(string &s, string &p) {
		// write your code here
		vector<int> v;
		unordered_map<char,int> match, dict;
		for(int i=p.length(); i--; dict[p[i]]++);
		for(int i=0; i<s.length(); ++i){
			match[s[i]]++;
			if(i>=p.length()-1){
				if(ismatch(match, dict)) v.push_back(i-p.length()+1);
				match[s[i-p.length()+1]]--;
				if(match[s[i-p.length()+1]]==0) match.erase(s[i-p.length()+1]);
			}
		}
		return v;
	}
};
