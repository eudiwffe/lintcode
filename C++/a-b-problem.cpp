/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/a-b-problem
   @Language: C++
   @Datetime: 16-02-09 04:39
   */

class Solution {
public:
	/*
	 * @param a: The first integer
	 * @param b: The second integer
	 * @return: The sum of a and b
	 */
	int aplusb(int a, int b) {
		// write your code here, try to do it without arithmetic operators.
		for(int c; b; b=c<<1){
			c = a&b;	// carry
			a ^= b;		// remainder
		}
		return a;
	}
};
