/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/poor-pigs
   @Language: C++
   @Datetime: 19-05-22 10:22
   */

class Solution {
public:
	/**
	 * @param buckets: an integer
	 * @param minutesToDie: an integer
	 * @param minutesToTest: an integer
	 * @return: how many pigs you need to figure out the "poison" bucket within p minutes 
	 */
	int poorPigs(int buckets, int minutesToDie, int minutesToTest) {
		// Write your code here
		float k=log(buckets)/log(minutesToTest/minutesToDie+1);
		return k>(int)k?k+1:k;
	}
};
