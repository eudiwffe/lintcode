/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/distribute-candies
   @Language: C++
   @Datetime: 19-05-15 15:46
   */

class Solution {
public:
	/**
	 * @param candies: a list of integers
	 * @return: return a integer
	 */
	int distributeCandies(vector<int> &candies) {
		// write your code here
		unordered_set<int> s(candies.begin(),candies.end());
		return min(candies.size()/2,s.size());
	}
};
