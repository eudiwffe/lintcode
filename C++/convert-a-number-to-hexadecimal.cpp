/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/convert-a-number-to-hexadecimal
   @Language: C++
   @Datetime: 19-05-23 10:22
   */

// Method 1, call sprintf, Time 50ms
class Solution {
public:
	/**
	 * @param num: an integer
	 * @return: convert the integer to hexadecimal
	 */
	string toHex(int num) {
		// Write your code here
		char str[32];
		sprintf(str,"%x",num);
		return str;
	}
};


// Method 2, do not call function, Time 50ms
class Solution {
public:
	/**
	 * @param num: an integer
	 * @return: convert the integer to hexadecimal
	 */
	string toHex(int num) {
		// Write your code here
		if(num==0) return "0";
		const char hex[]="0123456789abcdef";
		string str;
		for(int i=0; i<32 && num; i+=4, num>>=4)
			str.push_back(hex[num&0xF]);
		reverse(str.begin(),str.end());
		return str;
	}
};
