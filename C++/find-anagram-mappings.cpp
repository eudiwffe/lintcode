/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-anagram-mappings
   @Language: C++
   @Datetime: 19-04-27 13:49
   */

class Solution {
public:
	/**
	 * @param A: lists A
	 * @param B: lists B
	 * @return: the index mapping
	 */
	vector<int> anagramMappings(vector<int> &A, vector<int> &B) {
		// Write your code here
		unordered_map<int,int> dict;
		for(int i=B.size(); i--; dict[B[i]]=i);
		vector<int> res(A.size(),0);
		for(int i=A.size(); i--; res[i]=dict[A[i]]);
		return res;
	}
};
