/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/flip-bits
   @Language: C++
   @Datetime: 16-02-09 04:48
   */

class Solution {
public:
	/**
	 * @param a, b: Two integer
	 * return: An integer
	 */
	int bitSwapRequired(int a, int b) {
		// write your code here
		int c=0;
		for(a^=b; a; a&=a-1,++c);
		return c;
	}
};
