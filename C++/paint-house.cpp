/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/paint-house
   @Language: C++
   @Datetime: 19-06-03 16:13
   */

class Solution {
public:
	/**
	 * @param costs: n x 3 cost matrix
	 * @return: An integer, the minimum cost to paint all houses
	 */
	int minCost(vector<vector<int> > &costs) {
		// write your code here
		if(costs.size()==0) return 0;
		int red=costs[0][0], blue=costs[0][1], green=costs[0][2];
		for(int i=1; i<costs.size(); ++i){
			int r=red, b=blue, g=green;
			red=min(b,g)+costs[i][0];
			blue=min(r,g)+costs[i][1];
			green=min(r,b)+costs[i][2];
		}
		return min(red,min(blue,green));
	}
};
