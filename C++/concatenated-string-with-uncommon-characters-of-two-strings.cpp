/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/concatenated-string-with-uncommon-characters-of-two-strings
   @Language: C++
   @Datetime: 19-04-25 13:46
   */

class Solution {
public:
	/**
	 * @param s1: the 1st string
	 * @param s2: the 2nd string
	 * @return: uncommon characters of given strings
	 */
	string concatenetedString(string &s1, string &s2) {
		// write your code here
		unordered_set<char> dict(s2.begin(),s2.end()), del;
		string res;
		for(const char &c:s1){
			if(dict.count(c)) del.insert(c);
			else res.push_back(c);
		}
		for(const char &c:s2)
			if(del.count(c)<1) res.push_back(c);
		return res;
	}
};
