/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/range-addition-ii
   @Language: C++
   @Datetime: 19-05-15 11:47
   */

class Solution {
public:
	/**
	 * @param m: an integer
	 * @param n: an integer
	 * @param ops: List[List[int]]
	 * @return: return an integer
	 */
	int maxCount(int m, int n, vector<vector<int> > &ops) {
		// write your code here
		int row=m, col=n;
		for(const auto &v:ops){
			row=min(row,v[0]);
			col=min(col,v[1]);
		}
		return row*col;
	}
};
