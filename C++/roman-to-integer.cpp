/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/roman-to-integer
   @Language: C++
   @Datetime: 17-04-15 11:30
   */

class Solution {
public:
	/**
	 * @param s Roman representation
	 * @return an integer
	 * Tip: See http://baike.baidu.com/view/42061.htm
	 */
	int romanToInt(string& s) {
		// Write your code here
		unordered_map<char,int> roman={{'I',1},{'V',5},{'X',10}
			,{'L',50},{'C',100},{'D',500},{'M',1000}};
		int ans=0;
		for(int i=s.size(); i--;){
			if (i+1<s.size() && roman[s[i]]<roman[s[i+1]])
				ans -= roman[s[i]];
			else ans+= roman[s[i]];
		}
		return ans;
	}
};
