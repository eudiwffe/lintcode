/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/assignment-operator-overloading-c-only
   @Language: C++
   @Datetime: 16-02-09 05:42
   */

class Solution {
public:
	char *m_pData;
	Solution() {
		this->m_pData = NULL;
	}
	Solution(char *pData) {
		this->m_pData = pData;
	}

	// Implement an assignment operator
	// no destructor
	// Solution s(pData), a;
	// Using s=a can clear memory without leak
	Solution operator=(const Solution &object) {
		// write your code here
		if (this == &object) return *this;
		if (m_pData) delete[] m_pData;
		m_pData = NULL;
		if (object.m_pData){
			m_pData = new char[strlen(object.m_pData)];
			copy(object.m_pData,object.m_pData+strlen(object.m_pData),m_pData);
		}
		return *this;
	}
};
