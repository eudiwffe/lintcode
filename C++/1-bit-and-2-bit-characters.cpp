/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/1-bit-and-2-bit-characters
   @Language: C++
   @Datetime: 19-03-11 15:58
   */

class Solution {
public:
	/**
	 * @param bits: a array represented by several bits. 
	 * @return: whether the last character must be a one-bit character or not
	 * Tips:
	 * 1 bit may 1 or 0, 2 bit may 10, 11
	 */
	bool isOneBitCharacter(vector<int> &bits) {
		if (bits.size()<2) return true;
		return bits[bits.size()-2]==0;
	}
};
