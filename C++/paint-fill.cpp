/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/paint-fill
   @Language: C++
   @Datetime: 20-01-10 09:55
   */

class Solution {
public:
	/**
	 * @param screen: a two-dimensional array of colors
	 * @param x: the abscissa of the specified point
	 * @param y: the ordinate of the specified point
	 * @param newColor: the specified color
	 * @return: Can it be filled
	 */
	bool paintFill(vector<vector<int>> &screen, int x, int y, int newColor) {
		// write your code here.
		return screen[x][y]!=newColor;
	}
};
