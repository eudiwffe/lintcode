/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-smallest-letter-greater-than-target
   @Language: C++
   @Datetime: 19-05-13 14:21
   */

class Solution {
public:
	/**
	 * @param letters: a list of sorted characters
	 * @param target: a target letter
	 * @return: the smallest element in the list that is larger than the given target
	 */
	char nextGreatestLetter(string &letters, char target) {
		// Write your code here
		for(int i=0; i<letters.length(); ++i)
			if(letters[i]>target) return letters[i];
		return letters[0];
	}
};
