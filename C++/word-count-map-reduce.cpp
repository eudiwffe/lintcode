/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/word-count-map-reduce
   @Language: C++
   @Datetime: 19-04-05 12:33
   */


/**
 * Definition of Input:
 * template<class T>
 * class Input {
 * public:
 *     bool done(); 
 *         // Returns true if the iteration has elements or false.
 *     void next();
 *         // Move to the next element in the iteration
 *         // Runtime error if the iteration has no more elements
 *     T value();
 *        // Get the current element, Runtime error if
 *        // the iteration has no more elements
 * }
 */

class WordCountMapper: public Mapper {
public:
	void Map(Input<string>* input) {
		// Write your code here
		// Please directly use func 'output' to 
		// output the results into output buffer.
		// void output(string &key, int value);
		for(string key; !input->done(); input->next())
			for(stringstream istr(input->value()); istr>>key;)
				output(key,1);
	}
};


class WordCountReducer: public Reducer {
public:
	void Reduce(string &key, Input<int>* input) {
		// Write your code here
		// Please directly use func 'output' to 
		// output the results into output buffer.
		// void output(string &key, int value);
		int count=0;
		for(; !input->done(); input->next())
			count+=input->value();
		output(key,count);
	}
};
