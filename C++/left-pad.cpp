/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/left-pad
   @Language: C++
   @Datetime: 16-12-07 08:24
   */

class StringUtils {
public:
	/*
	 * @param originalStr: the string we want to append to
	 * @param size: the target length of the string
	 * @param padChar: the character to pad to the left side of the string
	 * @return: A string
	 */
	static string leftPad(string &originalStr, int size, char padChar=' ') {
		// write your code here
		if(size<=originalStr.length()) return originalStr;
		else return string(size-originalStr.length(), padChar)+originalStr;
	}
};
