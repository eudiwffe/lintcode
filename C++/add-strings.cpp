/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/add-strings
   @Language: C++
   @Datetime: 19-03-28 14:40
   */

class Solution {
public:
	/**
	 * @param num1: a non-negative integers
	 * @param num2: a non-negative integers
	 * @return: return sum of num1 and num2
	 */
	string addStrings(string &num1, string &num2) {
		// write your code here
		int n=num1.length(), m=num2.length();
		string res(max(n,m)+1,'0');
		for(int i=n, j=m, k=max(n,m), carry=0; i || j || carry;){
			if (i) carry += num1[--i]-'0';
			if (j) carry += num2[--j]-'0';
			res[k--]=carry%10+'0';
			carry /=10;
		}
		if (res[0]=='0') res.erase(0,1);
		return res;
	}
};
