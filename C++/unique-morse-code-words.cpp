/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/unique-morse-code-words
   @Language: C++
   @Datetime: 19-05-09 10:42
   */

class Solution {
public:
	/**
	 * @param words: the given list of words
	 * @return: the number of different transformations among all words we have
	 */
	int uniqueMorseRepresentations(vector<string> &words) {
		// Write your code here
		const string morse[26]={".-","-...","-.-.","-..",".","..-.","--.","....",
			"..",".---","-.-",".-..","--","-.","---",".--.",
			"--.-",".-.","...","-","..-","...-",".--","-..-",
			"-.--","--.."};
		unordered_set<string> dict;
		for(const string &word:words){
			string str;
			for(const char &c:word)
				str.append(morse[c-'a']);
			dict.insert(str);
		}
		return dict.size();
	}
};
