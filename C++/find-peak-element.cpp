/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-peak-element
   @Language: C++
   @Datetime: 16-02-09 05:49
   */

class Solution {
public:
	/**
	 * @param A: An integers array.
	 * @return: return any of peek positions.
	 */
	int findPeak(vector<int> A) {
		// write your code here
		int begin=0, end=A.size(), mid;
		while(begin<end){
			mid = begin+(end-begin)/2;
			if ((mid==0||A[mid-1]<A[mid])
					&& (mid+1==A.size()||A[mid+1]<A[mid]))
				return mid;
			if (mid==0 || A[mid-1]<A[mid]) begin=mid+1;
			else end=mid;
		}
		return begin;
	}
};
