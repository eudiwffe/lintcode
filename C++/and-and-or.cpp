/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/and-and-or
   @Language: C++
   @Datetime: 19-06-25 09:46
   */


class Solution {
public:
	/**
	 * @param n: 
	 * @param nums: 
	 * @return: return the sum of maximum OR sum, minimum OR sum, maximum AND sum, minimum AND sum.
	 */
	long long getSum(int n, vector<int> &nums) {
		// write your code here
		if(nums.size()==0) return 0;
		long long max_or=nums[0], min_or=nums[0], max_and=nums[0],min_and=nums[0];
		for(const auto &num:nums){
			max_or |= num;
			min_or=min(min_or,(long long)num);
			max_and=max(max_and,(long long)num);
			min_and &= num;
		}
		return max_or+min_or+max_and+min_and;
	}
};
