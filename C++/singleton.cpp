/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/singleton
   @Language: C++
   @Datetime: 16-02-09 05:18
   */

class Solution {
	Solution(){}
	Solution(const Solution &){}
	Solution operator=(const Solution &){}

public:
	/**
	 * @return: The same instance of this class every time
	 * Tip : C++ thread safe, lazy initialization, without memory leak
	 */
	static Solution* getInstance() {
		// write your code here
		static Solution s;
		return &s;
	}
};
