/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/is-possible
   @Language: C++
   @Datetime: 19-05-28 15:11
   */

class Solution {
public:
	/**
	 * @param a: 
	 * @param b: 
	 * @param c: 
	 * @param d: 
	 * @return: return "Yes" or "No"
	 */
	string IsPossible(int a, int b, int c, int d) {
		// write your code here
		if(a==c && b==d) return "Yes";
		if(a>c || b>d) return "No";
		return (IsPossible(a+b,b,c,d).compare("Yes")==0 ||
				IsPossible(a,a+b,c,d).compare("Yes")==0)
			? "Yes" : "No";
	}
};
