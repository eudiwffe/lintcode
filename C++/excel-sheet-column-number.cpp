/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/excel-sheet-column-number
   @Language: C++
   @Datetime: 19-05-27 15:58
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: return a integer
	 */
	int titleToNumber(string &s) {
		// write your code here
		int col=0;
		for(int i=0; i<s.length(); col=col*26+s[i++]-'A'+1);
		return col;
	}
};
