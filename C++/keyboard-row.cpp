/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/keyboard-row
   @Language: C++
   @Datetime: 19-05-16 14:36
   */


class Solution {
public:
	/**
	 * @param words: a list of strings
	 * @return: return a list of strings
	 */
	vector<string> findWords(vector<string> &words) {
		// write your code here
		const char key[26][3]={"a1","b2","c2","d1","e0","f1","g1","h1","i0","j1",
			"k1","l1","m2","n2","o0","p0","q0","r0","s1","t0",
			"u0","v2","w0","x2","y0","z2"};
		vector<string> vs;
		for(const string &word:words){
			bool found=true;
			for(int i=0, c[3]={0}; found && i<word.length(); ++i){
				++c[key[tolower(word[i])-'a'][1]-'0'];
				if((c[0]&&c[1]) || (c[0]&&c[2]) || (c[1]&&c[2])) found=false;
			}
			if(found) vs.push_back(word);
		}
		return vs;
	}
};
