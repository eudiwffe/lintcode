/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/to-lower-case
   @Language: C++
   @Datetime: 19-05-28 11:33
   */

class Solution {
public:
	/**
	 * @param str: the input string
	 * @return: The lower case string
	 */
	string toLowerCase(string &str) {
		// Write your code here
		for(int i=str.length(); i--; str[i]=tolower(str[i]));
		return str;
	}
};
