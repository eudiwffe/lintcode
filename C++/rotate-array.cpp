/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rotate-array
   @Language: C++
   @Datetime: 19-03-25 11:25
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @param k: an integer
	 * @return: rotate the array to the right by k steps
	 */
	vector<int> rotate(vector<int> &nums, int k) {
		// Write your code here
		k = k%nums.size();
		if (k==0) return nums;
		reverse(nums.begin(),nums.end()-k);
		reverse(nums.end()-k,nums.end());
		reverse(nums.begin(),nums.end());
		return nums;
	}
};
