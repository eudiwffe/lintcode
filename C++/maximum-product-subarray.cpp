/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/maximum-product-subarray
   @Language: C++
   @Datetime: 16-02-09 08:17
   */

class Solution {
public:
	/**
	 * @param nums: An array of integers
	 * @return: An integer
	 */
	int maxProduct(vector<int> &nums) {
		// write your code here
		if(nums.size()<1) return 0;
		int maxs=nums[0], mins=nums[0], res=nums[0];
		for(int i=1; i<nums.size(); ++i){
			int a=maxs*nums[i], b=mins*nums[i];
			maxs=max(nums[i],max(a,b));
			mins=min(nums[i],min(a,b));
			res=max(res,maxs);
		}
		return res;
	}
};
