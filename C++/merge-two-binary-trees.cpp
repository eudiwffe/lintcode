/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/merge-two-binary-trees
   @Language: C++
   @Datetime: 19-05-14 15:51
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param t1: the root of the first tree
	 * @param t2: the root of the second tree
	 * @return: the new binary tree after merge
	 */
	TreeNode * mergeTrees(TreeNode * t1, TreeNode * t2) {
		// Write your code here
		if(t1==NULL && t2==NULL) return NULL;
		if(t1==NULL || t2==NULL) return t1?t1:t2;
		if(t1 && t2) t1->val+=t2->val;
		t1->left=mergeTrees(t1->left,t2->left);
		t1->right=mergeTrees(t1->right,t2->right);
		return t1;
	}
};
