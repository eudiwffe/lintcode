/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-upside-down
   @Language: C++
   @Datetime: 19-06-12 15:08
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: the root of binary tree
	 * @return: new root
	 */
	TreeNode * upsideDownBinaryTree(TreeNode * root) {
		// write your code here
		if(root==NULL || root->left==NULL) return root;
		TreeNode *left=root->left, *right=root->right;
		TreeNode *top=upsideDownBinaryTree(left);
		root->left=root->right=NULL;
		left->left=right;
		left->right=root;
		return top;
	}
};
