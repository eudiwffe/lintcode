/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/strobogrammatic-number
   @Language: C++
   @Datetime: 19-04-19 10:02
   */

class Solution {
public:
	/**
	 * @param num: a string
	 * @return: true if a number is strobogrammatic or false
	 */
	bool isStrobogrammatic(string &num) {
		// write your code here
		unordered_map<char,char> mirrors={{'0','0'},{'1','1'},{'6','9'},{'8','8'},{'9','6'}};
		for(int i=0, j=num.length(); j--; ++i){
			if(mirrors.count(num[j])<1) return false;
			if(mirrors[num[i]]!=num[j]) return false;
		}
		return true;
	}
};
