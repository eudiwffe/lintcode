/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/remove-duplicates-from-sorted-list
   @Language: C++
   @Datetime: 16-02-09 05:15
   */

/**
 * Definition of ListNode
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *         this->val = val;
 *         this->next = NULL;
 *     }
 * }
 */
class Solution {
public:
	/**
	 * @param head: The first node of linked list.
	 * @return: head node
	 */
	ListNode *deleteDuplicates(ListNode *head) {
		// write your code here
		for(ListNode *p=head,*q; p && (q=p->next);){
			if (p->val == q->val){
				p->next = q->next;
				delete q;	// recommend delete, although not can also ac!
			}
			else p = p->next;
		}
		return head;
	}
};
