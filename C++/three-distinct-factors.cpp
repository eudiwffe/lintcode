/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/three-distinct-factors
   @Language: C++
   @Datetime: 19-03-28 17:40
   */

class Solution {
public:
	/**
	 * @param n: the given number
	 * @return:  return true if it has exactly three distinct factors, otherwise false
	 * Tip:
	 * only prime num^2 has 3 distinct factors
	 */
	bool isThreeDisctFactors(long long n) {
		// write your code here
		long long x=sqrt((long double)n);
		if (x*x!=n) return false;
		for(int i=2; i<sqrt(x); ++i)
			if (x%i==0) return false;
		return true;;
	}
};
