/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/flatten-list
   @Language: C++
   @Datetime: 16-12-07 07:58
   */

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer,
 *     // rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds,
 *     // if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds,
 *     // if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */
class Solution {
public:
	// @param nestedList a list of NestedInteger
	// @return a list of integer
	vector<int> flatten(const vector<NestedInteger> &nestedList) {
		// Write your code here
		vector<int> v;
		for(int i=0; i<nestedList.size(); ++i)
			if (nestedList[i].isInteger())
				v.push_back(nestedList[i].getInteger());
			else{
				vector<int> t = flatten(nestedList[i].getList());
				v.insert(v.end(),t.begin(),t.end());
			}
		return v;
	}
};
