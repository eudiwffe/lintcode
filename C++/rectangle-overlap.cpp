/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rectangle-overlap
   @Language: C++
   @Datetime: 19-04-22 17:24
   */

/**
 * Definition for a point.
 * struct Point {
 *     int x;
 *     int y;
 *     Point() : x(0), y(0) {}
 *     Point(int a, int b) : x(a), y(b) {}
 * };
 */

class Solution {
	bool overlapxy(int a, int b, int c, int d){
		return (c>=a && c<=b) || (d>=a && d<=b)
			|| (a>=c && a<=d) || (b>=c && b<=d);
	}
public:
	/**
	 * @param l1: top-left coordinate of first rectangle
	 * @param r1: bottom-right coordinate of first rectangle
	 * @param l2: top-left coordinate of second rectangle
	 * @param r2: bottom-right coordinate of second rectangle
	 * @return: true if they are overlap or false
	 */
	bool doOverlap(Point &l1, Point &r1, Point &l2, Point &r2) {
		// write your code here
		return overlapxy(l1.x, r1.x, l2.x, r2.x) 
			&& overlapxy(r1.y, l1.y, r2.y, l2.y);
	}
};
