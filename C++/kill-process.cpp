/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/kill-process
   @Language: C++
   @Datetime: 19-04-28 17:45
   */


class Solution {
public:
	/**
	 * @param pid: the process id
	 * @param ppid: the parent process id
	 * @param kill: a PID you want to kill
	 * @return: a list of PIDs of processes that will be killed in the end
	 */
	vector<int> killProcess(vector<int> &pid, vector<int> &ppid, int kill) {
		// Write your code here
		unordered_multimap<int,int> dict;   // f,c
		for(int i=0; i<ppid.size(); ++i)
			dict.insert(make_pair(ppid[i],pid[i]));
		vector<int> res;
		queue<int> kills;
		for(kills.push(kill); kills.size(); kills.pop()){
			kill = kills.front();
			res.push_back(kill);
			for(auto r=dict.equal_range(kill); r.first!=r.second; ++r.first)
				kills.push(r.first->second);
		}
		return res;
	}
};
