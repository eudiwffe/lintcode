/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/sum-of-first-k-even-length-palindrome-numbers
   @Language: C++
   @Datetime: 19-06-17 10:30
   */

class Solution {
public:
	/**
	 * @param k: Write your code here
	 * @return: the sum of first k even-length palindrome numbers
	 */
	int sumKEven(int k) {
		// 
		int sum=0;
		for(int i=1; i<=k; ++i){
			string s=to_string(i);
			string r=s;
			reverse(r.begin(),r.end());
			s.append(r);
			sum+=stoi(s);
		}
		return sum;
	}
};
