/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/average-of-levels-in-binary-tree
   @Language: C++
   @Datetime: 19-05-21 16:03
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: the binary tree of the  root
	 * @return: return a list of double
	 */
	vector<double> averageOfLevels(TreeNode * root) {
		// write your code here
		vector<double> vd;
		queue<TreeNode*> q;
		for(q.push(root); q.size();){
			int sum=0, size=q.size();
			for(int i=0; i++<size; q.pop()){
				const TreeNode *front=q.front();
				sum+=front->val;
				if(front->left) q.push(front->left);
				if(front->right) q.push(front->right);
			}
			vd.push_back(1.0*sum/size);
		}
		return vd;
	}
};
