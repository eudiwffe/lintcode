/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/self-dividing-numbers
   @Language: C++
   @Datetime: 19-04-19 09:51
   */

class Solution {
	bool judge(int a){
		for(int k=a; k>0; k/=10){
			int d=k%10;
			if(d==0 || a%d!=0) return false;
		}
		return true;
	}
public:
	/**
	 * @param lower: Integer : lower bound
	 * @param upper: Integer : upper bound
	 * @return: a list of every possible Digit Divside Numbers
	 */
	vector<int> digitDivideNums(int lower, int upper) {
		// write your code here
		vector<int> v;
		for(int i=lower; i<=upper && i>0; ++i)
			if(judge(i)) v.push_back(i);
		return v;
	}
};
