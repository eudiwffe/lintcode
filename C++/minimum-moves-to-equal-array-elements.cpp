/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-moves-to-equal-array-elements
   @Language: C++
   @Datetime: 19-05-22 11:28
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @return: the minimum number of moves required to make all array elements equal
	 */
	int minMoves(vector<int> &nums) {
		// Write your code here
		sort(nums.begin(),nums.end());
		int step=0;
		for(int i=nums.size(); i--; step+=nums[i]-nums[0]);
		return step;
	}
};
