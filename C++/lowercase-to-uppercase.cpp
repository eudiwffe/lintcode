/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/lowest-common-ancestor
   @Language: C++
   @Datetime: 19-03-12 15:58
   */

class Solution {
public:
	/**
	 * @param character: a character
	 * @return: a character
	 */
	char lowercaseToUppercase(char character) {
		// write your code here
		if(character>='a' && character<='z')
			return character-'a'+'A';
		return character;
	}
};
