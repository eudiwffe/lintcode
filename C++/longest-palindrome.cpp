/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-palindromic
   @Language: C++
   @Datetime: 19-04-22 17:33
   */

class Solution {
public:
	/**
	 * @param s: a string which consists of lowercase or uppercase letters
	 * @return: the length of the longest palindromes that can be built
	 */
	int longestPalindrome(string &s) {
		// write your code here
		int hash[128]={0};
		int len=0;
		for(const char &c:s){
			if(hash[c]){
				len+=2;
				hash[c]=0;
			}
			else ++hash[c];
		}
		for(int i=0; i<128; ++i)
			if(hash[i]) return len+1;
		return len;
	}
};
