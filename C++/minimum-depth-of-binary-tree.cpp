/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-depth-of-binary-tree
   @Language: C++
   @Datetime: 16-02-09 04:52
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: The root of binary tree
	 * @return: An integer
	 * Tip:
	 * left tree is NULL, return minDepth(right)+1
	 */
	int minDepth(TreeNode * root) {
		// write your code here
		if (root==NULL) return 0;
		int left=minDepth(root->left);
		int right=minDepth(root->right);
		if (left>0 && right>0) return min(left,right)+1;
		else return max(left,right)+1;
	}
};
