/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-absolute-file-path
   @Language: C++
   @Datetime: 19-06-12 11:50
   */

class Solution {
public:
	/**
	 * @param input: an abstract file system
	 * @return: return the length of the longest absolute path to file
	 */
	int lengthLongestPath(string &input) {
		// write your code here
		unordered_map<int,int> lens={{0,0}};
		int mx=0;
		for(int i=0, j=0, t=0; i<input.size(); i=j+2){
			for(t=0; i+1<input.size() && input[i]=='\\' && input[i+1]=='t'; ++t, i+=2);
			j=input.find('\\',i);
			if(j==input.npos) j=input.length();
			const string name=input.substr(i,j-i);
			lens[t+1]=lens[t]+name.length()+1;
			if(name.find('.')!=name.npos) mx=max(mx,lens[t+1]-1);
		}
		return mx;
	}
};
