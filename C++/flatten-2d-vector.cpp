/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/flatten-2d-vector
   @Language: C++
   @Datetime: 19-05-06 14:19
   */

class Vector2D {
	vector<vector<int> > *vec2d;
	int row, col;
public:
	Vector2D(vector<vector<int> > &vec2d) {
		// Initialize your data structure here
		this->vec2d=&vec2d;
		row=col=0;
	}

	int next() {
		// Write your code here
		return (*vec2d)[row][col++];
	}

	bool hasNext() {
		// Write your code here
		for(; row<vec2d->size(); ++row){
			if(col<(*vec2d)[row].size()) return true;
			else col=0;
		}
		return false;
	}
};

/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D i(vec2d);
 * while (i.hasNext()) cout << i.next();
 */
