/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-difference-between-bst-nodes
   @Language: C++
   @Datetime: 19-05-13 11:52
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	TreeNode *last=NULL;
	int minidiff=INT_MAX;
	void inorder(TreeNode *root){
		if(root==NULL) return;
		inorder(root->left);
		if(last && abs(root->val-last->val)<minidiff)
			minidiff=abs(root->val-last->val);
		last=root;
		inorder(root->right);
	}
public:
	/**
	 * @param root: the root
	 * @return: the minimum difference between the values of any two different nodes in the tree
	 */
	int minDiffInBST(TreeNode * root) {
		// Write your code here
		inorder(root);
		return minidiff;
	}
};
