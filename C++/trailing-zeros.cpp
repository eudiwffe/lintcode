/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/trailing-zeros
   @Language: C++
   @Datetime: 16-02-09 05:21
   */

class Solution {
public:
	/**
	 * param n : description of n
	 * return: description of return 
	 * Tip : only 5*2 can get tail 0,
	 *       statistics each num have factor 2 and 5 counts
	 *       answer is min(count2,count5)
	 *       only statistics count5 (count2 >> count5)
	 */
	long long trailingZeros(long long n) {
		long long c=0;
		for(c=0; n; c+=(n/=5));
		return c;
	}
};
