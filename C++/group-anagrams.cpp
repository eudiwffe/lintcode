/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/group-anagrams
   @Language: C++
   @Datetime: 19-04-24 16:27
   */

class Solution {
public:
	/**
	 * @param strs: the given array of strings
	 * @return: The anagrams which have been divided into groups
	 */
	vector<vector<string> > groupAnagrams(vector<string> &strs) {
		// write your code here
		vector<vector<string> > vs;
		unordered_map<string,vector<string> > ms;
		for(const string &str:strs){
			string s=str;
			sort(s.begin(),s.end());
			ms[s].push_back(str);
		}
		for(const auto &p:ms)
			vs.push_back(p.second);
		return vs;
	}
};
