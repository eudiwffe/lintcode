/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/count-1-in-binary
   @Language: C++
   @Datetime: 16-02-09 04:46
   */

class Solution {
public:
	/**
	 * @param num: an integer
	 * @return: an integer, the number of ones in num
	 */
	int countOnes(int num) {
		// write your code here
		int c=0;
		for(; num; num&=(num-1),c++);
		return c;
	}
};
