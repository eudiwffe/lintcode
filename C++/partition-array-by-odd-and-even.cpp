/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/partition-array-by-odd-and-even
   @Language: C++
   @Datetime: 16-02-09 05:13
   */

class Solution {
public:
	/**
	 * @param nums: a vector of integers
	 * @return: nothing
	 */
	void partitionArray(vector<int> &nums) {
		// write your code here
		for(int i=0, j=nums.size()-1; i<j;) {
			for(;i<j && nums[j]%2==0; --j);
			for(;i<j && nums[i]%2; ++i);
			if(i>j) break;
			swap(nums[i],nums[j]);
		}
	}
};
