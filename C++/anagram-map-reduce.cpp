/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/anagram-map-reduce
   @Language: C++
   @Datetime: 19-04-29 17:45
   */


/**
 * Definition of Input:
 * template<class T>
 * class Input {
 * public:
 *     bool done(); 
 *         // Returns true if the iteration has elements or false.
 *     void next();
 *         // Move to the next element in the iteration
 *         // Runtime error if the iteration has no more elements
 *     T value();
 *        // Get the current element, Runtime error if
 *        // the iteration has no more elements
 * }
 */

class AnagramMapper: public Mapper {
public:
	void Map(Input<string>* input) {
		// Write your code here
		// Please directly use func 'output' to output 
		// the results into output buffer.
		// void output(string &key, string& value);
		for(string str; !input->done(); input->next()){
			for(stringstream istr(input->value()); istr>>str;){
				string key=str;
				sort(key.begin(),key.end());
				output(key,str);
			}
		}
	}
};


class AnagramReducer: public Reducer {
public:
	void Reduce(string &key, Input<string>* input) {
		// Write your code here
		// Please directly use func 'output' to output 
		// the results into output buffer.
		// void output(string &key, vector<string>& value);
		vector<string> vs;
		for(; !input->done(); input->next())
			vs.push_back(input->value());
		output(key,vs);
	}
};
