/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-words-in-a-string-ii
   @Language: C++
   @Datetime: 19-03-25 11:17
   */

class Solution {
public:
	/**
	 * @param str: a string
	 * @return: return a string
	 */
	string reverseWords(string &str) {
		// write your code here
		for(int i=0, j; i<str.length();){
			j=str.find(' ',i);
			reverse(str.begin()+i,j<str.length()?str.begin()+j:str.end());
			i=str.find_first_not_of(' ',j);
		}
		reverse(str.begin(),str.end());
		return str;
	}
};
