/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/cosine-similarity
   @Language: C++
   @Datetime: 16-02-09 04:46
   */

class Solution {
public:
	/**
	 * @param A: An integer array.
	 * @param B: An integer array.
	 * @return: Cosine similarity.
	 */
	double cosineSimilarity(vector<int> A, vector<int> B) {
		// write your code here
		long long c=0, a=0, b=0;
		for(int i=0; i<A.size(); ++i){
			c+=A[i]*B[i];
			a+=A[i]*A[i];
			b+=B[i]*B[i];
		}
		return (a*b ? 1.0*c/(sqrt(a)*sqrt(b)) : 2);
	}
};
