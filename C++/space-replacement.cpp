/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/space-replacement
   @Language: C++
   @Datetime: 16-02-09 05:18
   */

class Solution {
public:
	/*
	 * @param string: An array of Char
	 * @param length: The true length of the string
	 * @return: The true length of new string
	 */
	int replaceBlank(char string[], int length) {
		// write your code here
		if (string==NULL || length==0) return 0;
		int blank=0;
		for(int i=length; i--; blank+=string[i]==' '?1:0);
		string[length+2*blank]='\0';
		for(int i=length+blank*2-1, j=length; j--; --i)
			if(string[j]!=' ') string[i]=string[j];
			else strncpy(string+(i-=2),"\%20",3);
		return length+2*blank;
	}
};  
