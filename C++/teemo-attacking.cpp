/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/teemo-attacking
   @Language: C++
   @Datetime: 19-05-15 10:19
   */

class Solution {
public:
	/**
	 * @param timeSeries: the Teemo's attacking ascending time series towards Ashe
	 * @param duration: the poisoning time duration per Teemo's attacking
	 * @return: the total time that Ashe is in poisoned condition
	 */
	int findPoisonedDuration(vector<int> &timeSeries, int duration) {
		// Write your code here
		if(timeSeries.size()<1) return 0;
		int second=duration;
		for(int i=1; i<timeSeries.size(); ++i){
			if(timeSeries[i-1]+duration<=timeSeries[i]) second+=duration;
			else second+=timeSeries[i]-timeSeries[i-1];
		}
		return second;
	}
};
