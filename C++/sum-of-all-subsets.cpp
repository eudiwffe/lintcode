/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/sum-of-all-subsets
   @Language: C++
   @Datetime: 19-04-25 14:38
   */

class Solution {
public:
	/**
	 * @param n: the given number
	 * @return: Sum of elements in subsets
	 */
	int subSum(int n) {
		// write your code here
		return pow(2,n-1)*(n+1)*n/2;
	}
};
