/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/simplify-path
   @Language: C++
   @Datetime: 16-02-09 08:32
   */

class Solution {
public:
	/**
	 * @param path the original path
	 * @return the simplified path
	 */
	string simplifyPath(string& path) {
		// Write your code here
		for(int i=path.length(); i--; path[i]=(path[i]=='/'?' ':path[i]));
		stringstream istr(path);
		string str;
		vector<string> vs;
		while(istr>>str){
			if (str==".") continue;
			if (str!="..") vs.push_back(str);
			else if (vs.size()) vs.erase(vs.end()-1);
		}
		str.clear();
		for(int i=0; i<vs.size(); str.append("/"+vs[i++]));
		return (vs.size() ? str : "/");
	}
};
