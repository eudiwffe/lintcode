/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/two-sum-iii-data-structure-design
   @Language: C++
   @Datetime: 19-04-26 09:41
   */

class TwoSum {
	unordered_map<int,int> data;
public:
	/**
	 * @param number: An integer
	 * @return: nothing
	 */
	void add(int number) {
		// write your code here
		data[number]++;
	}

	/**
	 * @param value: An integer
	 * @return: Find if there exists any pair of numbers which sum is equal to the value.
	 */
	bool find(int value) {
		// write your code here
		for(const auto &a:data){
			auto p=data.find(value-a.first);
			if(p==data.end()) continue;
			if(a.first*2!=value || p->second>1) return true;
		}
		return false;
	}
};
