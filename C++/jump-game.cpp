/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/jump-game
   @Language: C++
   @Datetime: 16-02-09 05:55
   */

// Method 1,  Greedy, Time 34ms
class Solution {
public:
	/**
	 * @param A: A list of integers
	 * @return: The boolean answer
	 */
	bool canJump(vector<int> A) {
		// write you code here
		int maxpos = 0;
		for(int i=0; i<A.size(); ++i){
			if (i>maxpos) return false;
			maxpos = max(maxpos,i+A[i]);
		}
		return true;
	}
};
