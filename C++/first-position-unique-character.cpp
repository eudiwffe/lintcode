/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/first-position-unique-character
   @Language: C++
   @Datetime: 19-03-17 14:47
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: it's index
	 */
	int firstUniqChar(string &s) {
		// write your code here
		int dict[128][2], index=s.length();   // count, index
		memset(dict, 0, sizeof(dict));
		for(int i=0; i<s.length(); ++i){
			if (dict[s[i]][0]==0) dict[s[i]][1]=i;
			dict[s[i]][0]++;
		}
		for(int i=0; i<128; ++i){
			if (dict[i][0]==1 && index>dict[i][1])
				index=dict[i][1];
		}
		return index==s.length()?-1:index;
	}
};
