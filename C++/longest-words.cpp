/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-words
   @Language: C++
   @Datetime: 16-02-09 04:50
   */

class Solution {
public:
	/**
	 * @param dictionary: a vector of strings
	 * @return: a vector of strings
	 */
	vector<string> longestWords(vector<string> &dictionary) {
		// write your code here
		int len=0, l=0;
		vector<string> vs;
		for(int i=0; i<dictionary.size(); ++i){
			l = dictionary[i].length();
			if (l>len){
				vs.clear();
				vs.push_back(dictionary[i]);
				len=l;
			}
			else if (l==len)
				vs.push_back(dictionary[i]);
		}
		return vs;
	}
};
