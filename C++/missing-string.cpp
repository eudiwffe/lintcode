/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/missing-string
   @Language: C++
   @Datetime: 19-03-13 16:27
   */

class Solution {
public:
	/**
	 * @param str1: a given string
	 * @param str2: another given string
	 * @return: An array of missing string
	 * Tip: No duplication
	 */
	vector<string> missingString(string &str1, string &str2) {
		// Write your code here
		unordered_set<string> dict;
		stringstream s1(str1), s2(str2);
		for(string s; s2>>s; dict.insert(s));
		vector<string> vs;
		for(string s; s1>>s;)
			if (dict.find(s)==dict.end())
				vs.push_back(s);
		return vs;
	}
};

