/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/goat-latin
   @Language: C++
   @Datetime: 19-05-28 11:00
   */

class Solution {
public:
	/**
	 * @param S: 
	 * @return: nothing
	 */
	string toGoatLatin(string &S) {
		// 
		string res, word, tail="a";
		const string vowels="aeiouAEIOU";
		for(stringstream istr(S); istr>>word; tail.push_back('a')){
			if(vowels.find(word[0])==vowels.npos){
				word.push_back(word[0]);
				word.erase(0,1);
			}
			word.append("ma"+tail);
			res.append(word+" ");
		}
		res.pop_back();
		return res;
	}
};
