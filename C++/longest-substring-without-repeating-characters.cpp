/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-substring-without-repeating-characters
   @Language: C++
   @Datetime: 16-02-09 05:57
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: an integer
	 */
	int lengthOfLongestSubstring(string &s) {
		// write your code here
		vector<int> chars(256,-1);
		int max=0;
		for(int i=-1, j=0; j<s.length(); ++j){
			i=i>chars[s[j]]?i:chars[s[j]];
			chars[s[j]]=j;
			max=max>j-i?max:j-i;
		}
		return max;
	}
};
