/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-the-celebrity
   @Language: C++
   @Datetime: 19-06-12 14:48
   */

// Forward declaration of the knows API.
bool knows(int a, int b);

class Solution {
public:
	/**
	 * @param n a party with n people
	 * @return the celebrity's label or -1
	 */
	int findCelebrity(int n) {
		// Write your code here
		int can=0;
		for(int i=1; i<n; ++i)
			if(knows(can,i)) can=i;
		for(int i=0; i<n; ++i){
			if(i==can) continue;
			if(!knows(i,can) || knows(can,i)) return -1;
		}
		return can;
	}
};
