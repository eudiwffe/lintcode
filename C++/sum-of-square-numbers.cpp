/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/sum-of-square-numbers
   @Language: C++
   @Datetime: 19-04-25 10:41
   */

class Solution {
public:
	/**
	 * @param num: the given number
	 * @return: whether whether there're two integers
	 */
	bool checkSumOfSquareNumbers(int num) {
		// write your code here
		if(num<0) return false;
		for(int i=0, j=sqrt(num); i<=j;){
			const int sum=i*i+j*j;
			if(sum==num) return true;
			if(sum>num) --j;
			else ++i;
		}
		return false;
	}
};
