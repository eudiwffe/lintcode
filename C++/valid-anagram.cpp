/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-anagram
   @Language: C++
   @Datetime: 16-02-09 05:22
   */

class Solution {
public:
	/**
	 * @param s: The first string
	 * @param t: The second string
	 * @return: true or false
	 */
	bool anagram(string &s, string &t) {
		// write your code here
		int letters[128]={0};
		for(int i=s.length(); i--; ++letters[s[i]]);
		for(int i=t.length(); i--; --letters[t[i]]);
		for(int i=128; i--;)
			if (letters[i]!=0) return false;
		return true;
	}
};
