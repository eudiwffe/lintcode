/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/unique-email-addresses
   @Language: C++
   @Datetime: 19-03-14 12:42
   */

class Solution {
public:
	/**
	 * @param emails: 
	 * @return: The number of the different email addresses
	 */
	int numUniqueEmails(vector<string> &emails) {
		// write your code here
		unordered_set<string> dict;
		for(string s:emails){
			size_t add=s.find('+'), at=s.find('@');
			string begin = s.substr(0, add<at?add:at);
			for(size_t dot=0; (dot=begin.find('.'))!=string::npos;)
				begin.erase(dot, 1);
			dict.insert(begin+s.substr(at));
		}
		return dict.size();
	}
};
