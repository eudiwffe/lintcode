/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/nth-to-last-node-in-list
   @Language: C++
   @Datetime: 16-02-09 05:13
   */

/**
 * Definition of ListNode
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *         this->val = val;
 *         this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param head: The first node of linked list.
	 * @param n: An integer.
	 * @return: Nth to last node of a singly linked list. 
	 */
	ListNode *nthToLast(ListNode *head, int n) {
		// write your code here
		ListNode *p;
		for(p=head; n--; p=p->next);
		for(; p; head=head->next, p=p->next);
		return head;
	}
};
