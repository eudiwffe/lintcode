/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-word-abbreviation
   @Language: C++
   @Datetime: 19-04-23 10:24
   */

class Solution {
public:
	/**
	 * @param word: a non-empty string
	 * @param abbr: an abbreviation
	 * @return: true if string matches with the given abbr or false
	 */
	bool validWordAbbreviation(string &word, string &abbr) {
		// write your code here
		for(int i=0, j=0; i<abbr.length();){
			if(!isdigit(abbr[i]) || abbr[i]=='0')
				if(j>=word.length() || abbr[i++]!=word[j++]) return false;
			j+=atoi(abbr.c_str()+i);
			if(j>word.length()) return false;
			if(abbr[i]=='0') continue;
			for(; isdigit(abbr[i]); ++i);
			if(i==abbr.length() && j<word.length()) return false;
		}
		return true;
	}
};
