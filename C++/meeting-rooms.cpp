/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/meeting-rooms
   @Language: C++
   @Datetime: 19-05-07 15:05
   */

/**
 * Definition of Interval:
 * classs Interval {
 *     int start, end;
 *     Interval(int start, int end) {
 *         this->start = start;
 *         this->end = end;
 *     }
 * }
 */

class Solution {
	static bool intervaless(const Interval &a, const Interval &b){
		return a.start<b.start;
	}
public:
	/**
	 * @param intervals: an array of meeting time intervals
	 * @return: if a person could attend all meetings
	 */
	bool canAttendMeetings(vector<Interval> &intervals) {
		// Write your code here
		sort(intervals.begin(),intervals.end(),intervaless);
		for(int i=1; i<intervals.size(); ++i)
			if(intervals[i-1].end>intervals[i].start) return false;
		return true;
	}
};
