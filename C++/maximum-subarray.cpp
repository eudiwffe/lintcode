/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/maximum-subarray
   @Language: C++
   @Datetime: 16-02-09 04:51
   */

class Solution {
public:    
	/**
	 * @param nums: A list of integers
	 * @return: A integer indicate the sum of max subarray
	 */
	int maxSubArray(vector<int> nums) {
		// write your code here
		int sum=0, mx=INT_MIN;
		for(int i=0; i<nums.size(); ++i){
			sum += nums[i];
			mx = max(mx,sum);
			if (sum<0) sum=0;
		}
		return mx;
	}
};
