/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/plus-one
   @Language: C++
   @Datetime: 16-02-09 05:14
   */

class Solution {
public:
	/**
	 * @param digits: a number represented as an array of digits
	 * @return: the result
	 */
	vector<int> plusOne(vector<int> &digits) {
		// write your code here
		vector<int> v(digits);
		int carry=1;
		for(int i=v.size(); i--;){
			carry += v[i];
			v[i] = carry%10;
			carry /=10;
		}
		if(carry) v.insert(v.begin(),carry);
		return v;
	}
};
