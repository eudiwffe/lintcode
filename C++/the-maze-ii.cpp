/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/the-maze-ii
   @Language: C++
   @Datetime: 19-06-25 16:06
   */

class Solution {
public:
	/**
	 * @param maze: the maze
	 * @param start: the start
	 * @param destination: the destination
	 * @return: the shortest distance for the ball to stop at the destination
	 */
	int shortestDistance(vector<vector<int>> &maze, vector<int> &start, vector<int> &destination) {
		// write your code here
		int row=maze.size(), col=maze[0].size(), cost=INT_MAX;
		queue<pair<long,int> > q;
		unordered_set<long> visit;
		for(q.push({((start[0]+0L)<<32)|start[1],0}); q.size(); q.pop()){
			const int i=q.front().first>>32, j=q.front().first&0xFFFFFFFF;
			if(i<0 || i>=row || j<0 || j>=col) continue;
			if(maze[i][j]==1) continue;
			if(i==destination[0] && j==destination[1]){
				cost=min(cost,q.front().second);
				continue;
			}
			if(visit.count(q.front().first)) continue;
			visit.insert(q.front().first);
			int r, c;
			for(r=i, c=j; r+1<row && maze[r+1][c]==0; ++r);
			q.push({((r+0L)<<32)|c,q.front().second+r-i});
			for(r=i, c=j; r-1>=0 && maze[r-1][c]==0; --r);
			q.push({((r+0L)<<32)|c,q.front().second+i-r});
			for(r=i, c=j; c+1<col && maze[r][c+1]==0; ++c);
			q.push({((r+0L)<<32)|c,q.front().second+c-j});
			for(r=i, c=j; c-1>=0 && maze[r][c-1]==0; --c);
			q.push({((r+0L)<<32)|c,q.front().second+j-c});
		}
		return cost==INT_MAX?-1:cost;
	}
};
