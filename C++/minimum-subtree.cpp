/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-subtree
   @Language: C++
   @Datetime: 19-04-08 15:24
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	int dfs(TreeNode *root, TreeNode **node, int &minsum){
		if(root==NULL) return 0;
		int sum=dfs(root->left,node,minsum)+dfs(root->right,node,minsum)+root->val;
		if(sum<minsum){
			minsum=sum;
			*node=root;
		}
		return sum;
	}
public:
	/**
	 * @param root: the root of binary tree
	 * @return: the root of the minimum subtree
	 */
	TreeNode * findSubtree(TreeNode * root) {
		// write your code here
		int sum=INT_MAX;
		TreeNode *node=NULL;
		dfs(root,&node,sum);
		return node;
	}
};
