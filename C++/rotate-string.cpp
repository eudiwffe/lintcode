/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rotate-string
   @Language: C++
   @Datetime: 16-02-09 05:17
   */

class Solution {
public:
	/**
	 * @param str: a string
	 * @param offset: an integer
	 * @return: nothing
	 */
	/** Tips
	 * From Linear Algebra, Matrix Transpose has rule as:
	 * T(T(A)) = A,          //T() means Transpose Matrix
	 * T(A*B) = T(B)*T(A)
	 * so, B*A = T(T(A)*T(B))
	 * follow this idea, rotate string[begin,end):
	 * step 1 : reverse string[begin,mid)
	 * step 2 : reverse string[mid,end)
	 * step 3 : reverse string[begin,end)
	 * */
	void rotateString(string &str,int offset){
		//wirte your code here
		if (str.length() < 1) return ;
		offset %= str.length();			// cutdown the move steps
		reverse(str.begin(), str.begin()+str.length()-offset);
		reverse(str.begin()+str.length()-offset, str.end());
		reverse(str.begin(), str.end());
	}
};
