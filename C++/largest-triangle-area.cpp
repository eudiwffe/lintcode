/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/largest-triangle-area
   @Language: C++
   @Datetime: 19-05-08 17:56
   */

class Solution {
	double getArea(int x1, int y1, int x2, int y2, int x3, int y3){
		double a=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
		double b=sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));
		double c=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
		double p=(a+b+c)/2;
		return sqrt(p*(p-a)*(p-b)*(p-c));
	}
public:
	/**
	 * @param points: List[List[int]]
	 * @return: return a double
	 */
	double largestTriangleArea(vector<vector<int> > &P) {
		// write your code here
		double area=0;
		for(int i=0; i<P.size(); ++i)
			for(int j=i+1; j<P.size(); ++j)
				for(int k=j+1; k<P.size(); ++k){
					double a=getArea(P[i][0],P[i][1],P[j][0],P[j][1],P[k][0],P[k][1]);
					area=max(area,a);
				}
		return area;
	}
};
