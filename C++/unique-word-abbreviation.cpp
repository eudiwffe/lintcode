/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/unique-word-abbreviation
   @Language: C++
   @Datetime: 19-04-24 09:53
   */

class ValidWordAbbr {
	unordered_map<string,int> abbrs;
	unordered_set<string> dict;
public:
	/*
	 * @param dictionary: a list of words
	 */
	ValidWordAbbr(vector<string> dictionary) {
		// do intialization if necessary
		char str[32];
		for(const string &word:dictionary){
			if(dict.count(word)) continue;
			if(word.length()<3) sprintf(str,"%s",word.c_str());
			else sprintf(str,"%c%d%c",word[0],word.length()-2,word.back());
			abbrs[str]++;
			dict.insert(word);
		}
	}

	/*
	 * @param word: a string
	 * @return: true if its abbreviation is unique or false
	 */
	bool isUnique(const string &word) {
		// write your code here
		char str[32];
		if(word.length()<3) sprintf(str,"%s",word.c_str());
		else sprintf(str,"%c%d%c",word[0],word.length()-2,word.back());
		return dict.count(word)>0 ? abbrs[str]==1:abbrs[str]==0;
	}
};

/**
 * Your ValidWordAbbr object will be instantiated and called as such:
 * ValidWordAbbr obj = new ValidWordAbbr(dictionary);
 * bool param = obj.isUnique(word);
 */
