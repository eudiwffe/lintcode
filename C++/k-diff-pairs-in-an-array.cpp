/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/k-diff-pairs-in-an-array
   @Language: C++
   @Datetime: 19-05-16 10:45
   */

class Solution {
public:
	/**
	 * @param nums: an array of integers
	 * @param k: an integer
	 * @return: the number of unique k-diff pairs
	 */
	int findPairs(vector<int> &nums, int k) {
		// Write your code here
		int count=0;
		if(k==0){
			unordered_map<int,int> freqs;
			for(int i=nums.size(); i--; ++freqs[nums[i]]);
			for(const auto &p:freqs)
				if(p.second>1) ++count;
		}
		else{
			unordered_set<int> dict;
			for(int i=nums.size(); i--; dict.insert(nums[i]));
			for(const int &a:dict)
				if(dict.count(a-k)) ++count;
		}
		return count;
	}
};
