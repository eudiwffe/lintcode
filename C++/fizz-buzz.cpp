/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/fizz-buzz
   @Language: C++
   @Datetime: 16-02-09 04:47
   */

class Solution {
public:
	/**
	 * param n: As description.
	 * return: A list of strings.
	 */
	/** Tips
	 * First judge divided 15(3*5)
	 * */
	vector<string> fizzBuzz(int n) {
		vector<string> vs;
		for (int i=1; i <= n; ++i) {
			if (i%15 == 0) vs.push_back("fizz buzz");
			else if (i%5 == 0) vs.push_back("buzz");
			else if (i%3 == 0) vs.push_back("fizz");
			else vs.push_back(to_string(i));
		}
		return vs;
	}
};
