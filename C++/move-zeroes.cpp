/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/move-zeroes
   @Language: C++
   @Datetime: 16-12-07 08:49
   */

class Solution {
public:
	/**
	 * @param nums an integer array
	 * @return nothing, do this in-place
	 */
	void moveZeroes(vector<int>& nums) {
		// Write your code here
		for(int i=0,j=0;i<nums.size() && j<nums.size();){
			for(;i<nums.size() && nums[i]!=0;++i);
			for(j=i+1;j<nums.size() && nums[j]==0;++j);
			if (i<nums.size() && j<nums.size())
				swap(nums[i++],nums[j++]);
		}
	}
};
