/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/product-of-array-except-itself
   @Language: C++
   @Datetime: 19-06-04 10:05
   */

// see problem 50 product-of-array-exclude-itself
class Solution {
public:
	/**
	 * @param nums: an array of integers
	 * @return: the product of all the elements of nums except nums[i].
	 */
	vector<int> productExceptSelf(vector<int> &nums) {
		// write your code here
		vector<int> res(nums.size(),1);
		for(int i=0; ++i<nums.size(); res[i]=res[i-1]*nums[i-1]);
		for(int i=nums.size(), r=1; i--; res[i]*=r, r*=nums[i]);
		return res;
	}
};
