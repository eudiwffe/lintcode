/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-uncommon-subsequence-i
   @Language: C++
   @Datetime: 19-05-16 11:20
   */

class Solution {
public:
	/**
	 * @param a: a string
	 * @param b: a string
	 * @return: return a integer
	 */
	int findLUSlength(string &a, string &b) {
		// write your code here
		return a==b?-1:max(a.length(),b.length());
	}
};
