/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/gray-code
   @Language: C++
   @Datetime: 17-04-27 16:27
   */

class Solution {
public:
	/**
	 * @param n a number
	 * @return Gray code
	 * Tip : num to gray, Gi = Bi ^ Bi+1
	 *       gray to num, Bi = Bi+1 ^ Gi, Bi+1 = Gi+1
	 */
	vector<int> grayCode(int n) {
		// Write your code here
		vector<int> v((1<<n),0);
		for(int i=1<<n; i--; v[i]=i^(i>>1));
		return v;
	}
};
