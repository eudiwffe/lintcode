/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/dota2-senate
   @Language: C++
   @Datetime: 19-05-15 11:01
   */

class Solution {
public:
	/**
	 * @param senate: a string
	 * @return: return a string
	 */
	string predictPartyVictory(string &senate) {
		// write your code here
		int n=senate.length();
		queue<int> r, d;
		for(int i=0; i<n; ++i)
			senate[i]=='R'?r.push(i):d.push(i);
		for(; r.size() && d.size();){
			int a=r.front(); r.pop();
			int b=d.front(); d.pop();
			a<b?r.push(a+n):d.push(b+n);
		}
		return r.size()?"Radiant":"Dire";
	}
};
