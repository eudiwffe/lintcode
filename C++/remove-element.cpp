/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/remove-element
   @Language: C++
   @Datetime: 16-02-09 05:16
   */

// Method 1, erase, Time 440ms
class Solution {
public:
	/** 
	 *@param A: A list of integers
	 *@param elem: An integer
	 *@return: The new length after remove
	 */
	int removeElement(vector<int> &A, int elem) {
		// write your code here
		vector<int>::iterator last,cur;
		for(last=cur=A.begin();cur!=A.end();++cur)
			if (*cur != elem){
				*last = *cur;
				++last;
			}
		A.erase(last,cur);
		return A.size();
	}
};

// Method 2, replace, Time 619ms
class Solution {
public:
	/*
	 * @param A: A list of integers
	 * @param elem: An integer
	 * @return: The new length after remove
	 */
	int removeElement(vector<int> &A, int elem) {
		// write your code here
		int len=0;
		for(int i=0; i<A.size(); ++i)
			if(A[i]!=elem) A[len++]=A[i];
		return len;
	}
};
