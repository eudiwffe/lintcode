/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/remove-linked-list-elements
   @Language: C++
   @Datetime: 16-02-09 04:39
   */

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
	/**
	 * @param head a ListNode
	 * @param val an integer
	 * @return a ListNode
	 */
	ListNode *removeElements(ListNode *head, int val) {
		// Write your code here
		ListNode H(-1), *p;
		for(p=&H; (p->next=head); head=p->next){
			if (head->val == val){
				p->next = head->next;
				delete head;
			}
			else p = head;
		}
		return H.next;
	}
};
