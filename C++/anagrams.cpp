/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/anagrams
   @Language: C++
   @Datetime: 16-02-15 05:40
   */

class Solution {
public:
	/**
	 * @param strs: A list of strings
	 * @return: A list of strings
	 */
	vector<string> anagrams(vector<string> &strs) {
		// write your code here
		unordered_map<string,int> dict;
		for(const string &str:strs){
			string tmp(str);
			sort(tmp.begin(),tmp.end());
			dict[tmp]++;
		}
		vector<string> vs;
		for(const auto &str:strs){
			string tmp(str);
			sort(tmp.begin(),tmp.end());
			if(dict[tmp]>1) vs.push_back(str);
		}
		return vs;
	}
};
