/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-palindrome
   @Language: C++
   @Datetime: 16-02-09 05:23
   */

class Solution {
public:
	/**
	 * @param s: A string
	 * @return: Whether the string is a valid palindrome
	 */
	bool isPalindrome(string &s) {
		// write your code here
		for(int i=0, j=s.length()-1; i<j; ++i, --j){
			for(; i<j && !isalnum(s[i]); ++i);
			for(; i<j && !isalnum(s[j]); --j);
			if(i<j && tolower(s[i])!=tolower(s[j])) return false;
		}
		return true;
	}
};
