/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/johns-backyard-garden
   @Language: C++
   @Datetime: 19-04-25 17:29
   */

// Method 1, backtrack
class Solution {
public:
	/**
	 * @param x: the wall's height
	 * @return: YES or NO
	 */
	string isBuild(int x) {
		// write you code here
		if(x%3==0 || x%7==0) return "YES";
		if(x>3 && isBuild(x-3)=="YES") return "YES";
		if(x>7 && isBuild(x-7)=="YES") return "YES";
		return "NO";
	}
};

// Method 2, enum
class Solution {
public:
	/**
	 * @param x: the wall's height
	 * @return: YES or NO
	 */
	string isBuild(int x) {
		// write you code here
		for(int i=0; i*7<=x; ++i)
			if((x-i*7)%3==0) return "YES";
		return "NO";
	}
};
