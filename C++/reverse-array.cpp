/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-array
   @Language: C++
   @Datetime: 19-03-25 11:35
   */

class Solution {
public:
	/**
	 * @param nums: a integer array
	 * @return: nothing
	 * Using STL reverse
	 */
	void reverseArray(vector<int> &nums) {
		// write your code here
		reverse(nums.begin(),nums.end());
	}
};


class Solution {
public:
	/**
	 * @param nums: a integer array
	 * @return: nothing
	 */
	void reverseArray(vector<int> &nums) {
		// write your code here
		//reverse(nums.begin(),nums.end());
		for(int i=0, j=nums.size()-1; i<j; ++i, --j)
			swap(nums[i],nums[j]);
	}
};
