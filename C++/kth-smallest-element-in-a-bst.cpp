/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/kth-smallest-element-in-a-bst
   @Language: C++
   @Datetime: 19-05-14 14:05
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	void inorder(TreeNode *root, int &k, int &kval){
		if(root==NULL || k==0) return;
		inorder(root->left,k,kval);
		--k;
		if(k==0) kval=root->val;
		inorder(root->right,k,kval);
	}
public:
	/**
	 * @param root: the given BST
	 * @param k: the given k
	 * @return: the kth smallest element in BST
	 */
	int kthSmallest(TreeNode * root, int k) {
		// write your code here
		int kval=0;
		inorder(root,k,kval);
		return kval;
	}
};
