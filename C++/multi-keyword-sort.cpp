/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/multi-keyword-sort
   @Language: C++
   @Datetime: 19-04-28 16:25
   */

class Solution {
	static bool comp(const vector<int> &a, const vector<int> &b){
		if(a[1]==b[1]) return a[0]<b[0];
		return a[1]>b[1];
	}
public:
	/**
	 * @param array: the input array
	 * @return: the sorted array
	 */
	vector<vector<int>> multiSort(vector<vector<int>> &array) {
		// Write your code here
		sort(array.begin(),array.end(),comp);
		return array;
	}
};
