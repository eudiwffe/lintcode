/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rearrange-a-string-with-integers
   @Language: C++
   @Datetime: 19-04-25 14:18
   */

class Solution {
public:
	/**
	 * @param str: a string containing uppercase alphabets and integer digits
	 * @return: the alphabets in the order followed by the sum of digits
	 */
	string rearrange(string &str) {
		// Write your code here
		int dict[128]={0};
		int sum=0;
		for(const char &c:str){
			if(isdigit(c)) sum+=c-'0';
			else dict[c]++;
		}
		string res;
		for(int i=0; i<128; ++i)
			for(int j=dict[i]; j--; res.push_back(i));
		if(str.length()) res.append(to_string(sum));
		return res;
	}
};
