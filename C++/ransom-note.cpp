/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/ransom-note
   @Language: C++
   @Datetime: 19-05-23 13:11
   */

class Solution {
public:
	/**
	 * @param ransomNote: a string
	 * @param magazine: a string
	 * @return: whether the ransom note can be constructed from the magazines
	 */
	bool canConstruct(string &ransomNote, string &magazine) {
		// Write your code here
		int hash[128]={0};
		for(int i=magazine.length(); i--; ++hash[magazine[i]]);
		for(int i=ransomNote.length(); i--;)
			if(hash[ransomNote[i]]<1) return false;
			else --hash[ransomNote[i]];
		return true;
	}
};
