/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/maximum-and-minimum
   @Language: C++
   @Datetime: 19-04-25 16:10
   */

class Solution {
public:
	/**
	 * @param matrix: an input matrix 
	 * @return: nums[0]: the maximum,nums[1]: the minimum
	 */
	vector<int> maxAndMin(vector<vector<int>> &matrix) {
		// write your code here
		if(matrix.size()<1 || matrix[0].size()<1) return {};
		int big=INT_MIN, small=INT_MAX;
		for(const auto &row:matrix){
			for(const auto &e:row){
				big=max(big,e);
				small=min(small,e);
			}
		}
		return {big,small};
	}
};
