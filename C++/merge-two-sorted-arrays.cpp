/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/merge-two-sorted-arrays
   @Language: C++
   @Datetime: 16-02-09 04:52
   */

class Solution {
public:
	/**
	 * @param A: sorted integer array A
	 * @param B: sorted integer array B
	 * @return: A new sorted integer array
	 */
	vector<int> mergeSortedArray(vector<int> &A, vector<int> &B) {
		// write your code here
		int m=A.size(), n=B.size();
		vector<int> v(m+n);
		for(int i=m+n; i--; v[i]=A[m-1]>B[n-1]?A[--m]:B[--n]);
		return v;
	}
};
