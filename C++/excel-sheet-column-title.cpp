/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/excel-sheet-column-title
   @Language: C++
   @Datetime: 19-05-27 16:26
   */

class Solution {
public:
	/**
	 * @param n: a integer
	 * @return: return a string
	 */
	string convertToTitle(int n) {
		// write your code here
		string s;
		for(; n; n=(n-1)/26)
			s.push_back((n-1)%26+'A');
		reverse(s.begin(),s.end());
		return s;
	}
};
