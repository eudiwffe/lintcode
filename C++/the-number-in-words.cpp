/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/the-number-in-words
   @Language: C++
   @Datetime: 19-06-19 10:50
   */

class Solution {
	string core(int n){
		static const string twenty[]={"","one","two","three","four","five","six",
			"seven","eight","nine","ten","eleven","twelf","thirteen","fourteen",
			"fifteen","sixteen","seventeen","eighteen","ninteen"};
		static const string hundred[]={"","ten","twenty","thirty","forty","fifty",
			"sixty","seventy","eighty","ninety"};
		static const string bit[]={" hundred "," thousand "," million "," billion "};
		if(n>=1e9) return core(n/((int)1e9))+bit[3]+core(n%((int)1e9));
		if(n>=1e6) return core(n/((int)1e6))+bit[2]+core(n%((int)1e6));
		if(n>=1e3) return core(n/((int)1e3))+bit[1]+core(n%((int)1e3));
		if(n>=1e2) return core(n/((int)1e2))+bit[0]+core(n%((int)1e2));
		if(n>=20 ) return hundred[n/10]+" "+core(n%10);
		return twenty[n];
	}
public:
	/**
	 * @param number: the number
	 * @return: the number in words
	 */
	string convertWords(int number) {
		// Write your code here
		if(number==0) return "zero";
		string str=core(number);
		if(str.back()==' ') str.pop_back();
		return str;
	}
};
