/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/calculate-maximum-value
   @Language: C++
   @Datetime: 19-04-17 9:54
   */

class Solution {
public:
	/**
	 * @param str: the given string
	 * @return: the maximum value
	 */
	int calcMaxValue(string &str) {
		// write your code here
		int sum=0;
		for(const char &c:str)
			sum=max(sum+c-'0',sum*(c-'0'));
		return sum;
	}
};
