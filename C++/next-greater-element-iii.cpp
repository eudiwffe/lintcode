/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/next-greater-element-iii
   @Language: C++
   @Datetime: 19-05-16 16:29
   */

class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: the smallest 32-bit integer which has exactly the same digits existing in the integer n and is greater in value than n
	 */
	int nextGreaterElement(int n) {
		// Write your code here
		string s=to_string(n);
		while(next_permutation(s.begin(),s.end()) && s[0]=='0');
		long l = atol(s.c_str());
		if(l>n && l<=INT_MAX) return l;
		else return -1;
	}
};
