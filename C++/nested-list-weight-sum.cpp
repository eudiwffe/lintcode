/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/nested-list-weight-sum
   @Language: C++
   @Datetime: 19-04-16 11:18
   */

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer,
 *     // rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds,
 *     // if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds,
 *     // if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */
class Solution {
	int depthSum(const NestedInteger &nl, int depth){
		if(nl.isInteger()) return nl.getInteger()*depth;
		int sum=0;
		for(const auto &i:nl.getList())
			sum+=depthSum(i,depth+1);
		return sum;
	}
public:
	int depthSum(const vector<NestedInteger>& nestedList) {
		// Write your code here
		int sum=0;
		for(const auto &nl:nestedList)
			sum+=depthSum(nl,1);
		return sum;
	}
};
