/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rotate-bits-left
   @Language: C++
   @Datetime: 19-06-25 09:40
   */

class Solution {
public:
	/**
	 * @param n: a number
	 * @param d: digit needed to be rorated
	 * @return: a number
	 */
	int leftRotate(int n, int d) {
		// write code here
		d%=32;
		return (n<<d)|((n>>(32-d))&((1<<(32-d))-1));
	}
};
