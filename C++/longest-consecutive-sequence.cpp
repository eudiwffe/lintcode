/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/longest-consecutive-sequence
   @Language: C++
   @Datetime: 16-02-09 05:57
   */

class Solution {
public:
	/**
	 * @param nums: A list of integers
	 * @return an integer
	 */
	int longestConsecutive(vector<int> &A) {
		// write you code here
		unordered_set<int> hs(A.begin(),A.end());
		int maxlen=0, low, high;
		while(hs.size()){
			const int a=*hs.begin();
			hs.erase(a);
			for(low=a-1; hs.count(low); hs.erase(low--));
			for(high=a+1; hs.count(high); hs.erase(high++));
			maxlen = max(maxlen,high-low-1);
		}
		return maxlen;
	}
};
