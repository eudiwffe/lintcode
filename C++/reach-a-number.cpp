/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reach-a-number
   @Language: C++
   @Datetime: 19-06-13 10:04
   */

class Solution {
public:
	/**
	 * @param target: the destination
	 * @return: the minimum number of steps
	 */
	int reachNumber(int target) {
		// Write your code here
		int sum=0, step=0, diff;
		target=abs(target);
		for(sum=0, step=1; sum<target; sum+=step++);
		diff=sum-target;
		if(diff&1) {
			if(step&1) return step;
			else return step+1;
		}
		else return step-1;
	}
};
