/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/largest-number
   @Language: C++
   @Datetime: 16-02-09 05:56
   */

class Solution {
	static bool strnumgreater(const int &i, const int &j){
		return to_string(i)+to_string(j)>to_string(j)+to_string(i);
	}

public:
	/**
	 *@param num: A list of non negative integers
	 *@return: A string
	 */
	string largestNumber(vector<int> &num) {
		// write your code here
		sort(num.begin(), num.end(), strnumgreater);
		string ans;
		for(int i=0; i<num.size(); ++i)
			ans.append(to_string(num[i]));
		if (ans.size()<1 || ans[0]=='0') return "0";
		return ans;
	}
};
