/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/compare-strings
   @Language: C++
   @Datetime: 16-02-09 04:46
   */

class Solution {
public:
	/**
	 * @param A: A string
	 * @param B: A string
	 * @return: if string A contains all of the characters in B return true else return false
	 */
	bool compareStrings(string &A, string &B) {
		// write your code here
		int letters[128]={0};
		for(int i=A.length(); i--; ++letters[A[i]]);
		for(int i=B.length(); i--;)
			if(letters[B[i]]--<1) return false;
		return true;
	}
};
