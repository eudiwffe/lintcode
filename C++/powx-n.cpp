/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/powx-n
   @Language: C++
   @Datetime: 16-02-09 08:22
   */

class Solution {
public:
	/**
	 * @param x the base number
	 * @param n the power number
	 * @return the result
	 */
	double myPow(double x, int n) {
		// Write your code here
		double res=1.0;
		for(int i=n; i!=0; x*=x, i/=2)
			if(i&1) res *= x;
		return n>0?res:1/res;
	}
};
