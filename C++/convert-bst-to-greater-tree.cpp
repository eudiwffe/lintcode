/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/convert-bst-to-greater-tree
   @Language: C++
   @Datetime: 19-04-25 10:08
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	int sum=0;
	void bfs(TreeNode *root){
		if(root==NULL) return;
		bfs(root->right);
		sum+=root->val;
		root->val=sum;
		bfs(root->left);
	}
public:
	/**
	 * @param root: the root of binary tree
	 * @return: the new root
	 */
	TreeNode * convertBST(TreeNode * root) {
		// write your code here
		bfs(root);
		return root;
	}
};
