/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/sum-of-left-leaves
   @Language: C++
   @Datetime: 19-05-23 11:16
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: t
	 * @return: the sum of all left leaves
	 */
	int sumOfLeftLeaves(TreeNode * root) {
		// Write your code here
		if(root==NULL) return 0;
		int left=sumOfLeftLeaves(root->left);
		int right=sumOfLeftLeaves(root->right);
		if(root->left && root->left->left==NULL && root->left->right==NULL)
			return left+right+root->left->val;
		else return left+right;
	}
};
