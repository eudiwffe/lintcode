/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/power-of-three
   @Language: C++
   @Datetime: 19-05-23 14:25
   */

// Method 1, log and power, 50ms
class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: if n is a power of three
	 */
	bool isPowerOfThree(int n) {
		// Write your code here
		if(n==0) return false;
		return n==(int)pow(3,(int)(log(n)/log(3)+0.5));
	}
};

