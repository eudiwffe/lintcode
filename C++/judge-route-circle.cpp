/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/judge-route-circle
   @Language: C++
   @Datetime: 19-05-14 14:30
   */

class Solution {
public:
	/**
	 * @param moves: a sequence of its moves
	 * @return: if this robot makes a circle
	 */
	bool judgeCircle(string &moves) {
		// Write your code here
		int ud=0, lr=0;
		for(const char &ch:moves){
			switch(ch){
				case 'U': ++ud; break;
				case 'D': --ud; break;
				case 'L': ++lr; break;
				case 'R': --lr; break;
			}
		}
		return ud==0 && lr==0;
	}
};
