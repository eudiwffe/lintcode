/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/two-sum-ii-input-array-is-sorted
   @Language: C++
   @Datetime: 19-04-26 16:21
   */

class Solution {
public:
	/**
	 * @param nums: an array of Integer
	 * @param target: target = nums[index1] + nums[index2]
	 * @return: [index1 + 1, index2 + 1] (index1 < index2)
	 */
	vector<int> twoSum(vector<int> &nums, int target) {
		// write your code here
		for(int i=0, j=nums.size()-1; i<j;)
			if(nums[i]+nums[j]==target) return {i+1,j+1};
			else if(nums[i]+nums[j]>target) --j;
			else ++i;
		return {};
	}
};
