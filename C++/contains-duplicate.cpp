/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/contains-duplicate
   @Language: C++
   @Datetime: 19-05-27 10:14
   */

class Solution {
public:
	/**
	 * @param nums: the given array
	 * @return: if any value appears at least twice in the array
	 */
	bool containsDuplicate(vector<int> &nums) {
		// Write your code here
		unordered_set<int> s;
		for(int i=nums.size(); i--; s.insert(nums[i]))
			if(s.count(nums[i])) return true;
		return false;
	}
};
