/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/perfect-number
   @Language: C++
   @Datetime: 19-05-16 13:52
   */

class Solution {
public:
	/**
	 * @param num: an integer
	 * @return: returns true when it is a perfect number and false when it is not
	 */
	bool checkPerfectNumber(int num) {
		// write your code here
		if(num==1) return false;
		int sum=1;
		for(int i=2; i<=sqrt(num); ++i)
			if(num%i==0) sum+=i+num/i;
		return sum==num;
	}
};
