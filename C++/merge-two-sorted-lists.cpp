/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/merge-two-sorted-lists
   @Language: C++
   @Datetime: 16-02-09 04:52
   */

/**
 * Definition of ListNode
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *         this->val = val;
 *         this->next = NULL;
 *     }
 * }
 */
class Solution {
public:
	/**
	 * @param ListNode l1 is the head of the linked list
	 * @param ListNode l2 is the head of the linked list
	 * @return: ListNode head of linked list
	 */
	/** Tips: all the list are single list without head node
	 *  It seems like the merge-two-sorted-arrays
	 * */
	ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
		// H is the virtual head for merged list
		ListNode H(-1), *p;
		for(p=&H; l1 && l2; p=p->next){
			if (l1->val < l2->val){
				p->next = l1;
				l1 = l1->next;
			}
			else{
				p->next = l2;
				l2 = l2->next;
			}
		}
		p->next = (l1 ? l1:l2);
		return H.next;
	}
};
