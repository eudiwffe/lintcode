/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/similar-rgb-color
   @Language: C++
   @Datetime: 19-05-13 11:02
   */

class Solution {
	int similar(int c){
		int a=0;
		for(int i=0; i<16; ++i){
			int ii=i|(i<<4);
			if(-(ii-c)*(ii-c)>-(a-c)*(a-c))
				a=ii;
		}
		return a;
	}
public:
	/**
	 * @param color: the given color
	 * @return: a 7 character color that is most similar to the given color
	 */
	string similarRGB(string &color) {
		// Write your code here
		int a,b,c;
		sscanf(color.c_str()+1,"%x",&a);
		c=a&0x0000FF, b=(a&0x00FF00)>>8, a=(a&0xFF0000)>>16;
		char tmp[10];
		sprintf(tmp,"#%02x%02x%02x",similar(a),similar(b),similar(c));
		return tmp;
	}
};
