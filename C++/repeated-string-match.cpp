/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/repeated-string-match
   @Language: C++
   @Datetime: 19-05-14 11:45
   */

class Solution {
public:
	/**
	 * @param A: a string
	 * @param B: a string
	 * @return: return an integer
	 */
	int repeatedStringMatch(string &A, string &B) {
		// write your code here
		int count=0;
		string str;
		for(; str.length()<B.length(); ++count)
			str.append(A);
		if(str.find(B)<str.length()) return count;
		str.append(A);
		++count;
		if(str.find(B)<str.length()) return count;
		return -1;
	}
};
