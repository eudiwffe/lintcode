/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-3-digit-integer
   @Language: C++
   @Datetime: 19-03-11 15:58
   */

class Solution {
public:
	/**
	 * @param number: A 3-digit number.
	 * @return: Reversed number.
	 */
	int reverseInteger(int number) {
		// write your code here
		int a = number/100;
		int b = (number-a*100)/10;
		int c = (number-a*100-b*10);
		return c*100+b*10+a;
	}
};
