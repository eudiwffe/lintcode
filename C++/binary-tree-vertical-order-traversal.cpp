/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-vertical-order-traversal
   @Language: C++
   @Datetime: 19-06-12 15:33
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param root: the root of tree
	 * @return: the vertical order traversal
	 */
	vector<vector<int> > verticalOrder(TreeNode * root) {
		// write your code here
		map<int,vector<int> > ver;
		queue<pair<TreeNode*,int> > que;
		for(que.push({root,0}); que.size(); que.pop()){
			const auto n=que.front();
			if(n.first==NULL) break;
			ver[n.second].push_back(n.first->val);
			if(n.first->left) que.push({n.first->left,n.second-1});
			if(n.first->right) que.push({n.first->right,n.second+1});
		}
		vector<vector<int> > vs;
		for(const auto &p:ver){
			vector<int> v;
			for(const auto &val:p.second)
				v.push_back(val);
			vs.push_back(v);
		}
		return vs;
	}
};
