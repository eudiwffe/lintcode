/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/repeated-substring-pattern
   @Language: C++
   @Datetime: 19-05-22 09:45
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: return a boolean
	 */
	bool repeatedSubstringPattern(string &s) {
		// write your code here
		string ss=s.substr(1)+s.substr(0,s.length()-1);
		return ss.find(s)!=ss.npos;
	}
};
