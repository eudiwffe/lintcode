/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/house-robber
   @Language: C++
   @Datetime: 16-02-09 05:52
   */

class Solution {
public:
	/**
	 * @param A: An array of non-negative integers.
	 * return: The maximum amount of money you can rob tonight
	 */
	long long houseRobber(vector<int> A) {
		// write your code here
		if (A.size()==0) return 0;
		long long pre=0,ans=A[0],temp;
		for(int i=1;i<A.size();++i){
			temp = max(pre+A[i],ans);
			pre = ans;
			ans = temp;
		}
		return ans;
	}
};
