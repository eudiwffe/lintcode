/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-order-storage
   @Language: C++
   @Datetime: 19-04-27 13:50
   */

/**
 * Definition of singly-linked-list:
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *        this->val = val;
 *        this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
	/**
	 * @param head: the given linked list
	 * @return: the array that store the values in reverse order 
	 */
	vector<int> reverseStore(ListNode * head) {
		// write your code here
		vector<int> v;
		for(; head; head=head->next)
			v.push_back(head->val);
		reverse(v.begin(),v.end());
		return v;
	}
};
