/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/best-time-to-buy-and-sell-stock-ii
   @Language: C++
   @Datetime: 16-02-09 05:43
   */

class Solution {
public:
	/**
	 * @param prices: Given an integer array
	 * @return: Maximum profit
	 */
	int maxProfit(vector<int> &prices) {
		// write your code here
		int profit=0;
		for(int i=1;i<prices.size();++i)
			if (prices[i]>prices[i-1])
				profit += prices[i]-prices[i-1];
		return profit;
	}
};
