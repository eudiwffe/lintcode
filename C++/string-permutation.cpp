/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/string-permutation
   @Language: C++
   @Datetime: 17-04-05 13:40
   */

class Solution {
public:
	/**
	 * @param A a string
	 * @param B a string
	 * @return a boolean
	 */
	bool stringPermutation(string& A, string& B) {
		// Write your code here
		if (A.size()!=B.size()) return false;
		vector<int> hm(256,0);
		for(int i=A.size(); i; ++hm[A[--i]]);
		for(int i=B.size(); i;){
			if (hm[B[--i]]) --hm[B[i]];
			else return false;
		}
		return true;
	}
};
