/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-number-with-alternating-bits
   @Language: C++
   @Datetime: 19-05-08 14:51
   */

class Solution {
public:
	/**
	 * @param n: a postive Integer
	 * @return: if two adjacent bits will always have different values
	 */
	bool hasAlternatingBits(int n) {
		// Write your code here
		for(int a=3; a<n; a<<=1)
			if((a&n)==a || (a&(a^n))==a) return false;
		return true;
	}
};
