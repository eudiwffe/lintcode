/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/last-digit-by-factorial-divide
   @Language: C++
   @Datetime: 19-07-03 14:09
   */

class Solution {
public:
	/**
	 * @param A: the given number
	 * @param B: another number
	 * @return: the last digit of B! / A! 
	 */
	int computeLastDigit(long long A, long long B) {
		// write your code here
		int bit=1;
		for(++A; A<=B && bit; ++A)
			bit=(bit*((int)(A%10)))%10;
		return bit;
	}
};
