/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/number-complement
   @Language: C++
   @Datetime: 19-05-22 09:49
   */


class Solution {
public:
	/**
	 * @param num: an integer
	 * @return: the complement number
	 */
	int findComplement(int num) {
		// Write your code here
		int x=1;
		for(; x<num; x=(x<<1)|1);
		return x-num;
	}
};
