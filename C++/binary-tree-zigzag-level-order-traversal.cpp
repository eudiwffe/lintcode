/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-zigzag-level-order-traversal
   @Language: C++
   @Datetime: 16-02-09 05:46
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	/**
	 * @param root: The root of binary tree.
	 * @return: A list of lists of integer include 
	 *          the zigzag level order traversal of its nodes' values 
	 */
public:
	vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
		// write your code here
		vector<vector<int> > vs;
		if (root==NULL) return vs;
		queue<TreeNode*> que;
		bool flag=true;
		for(que.push(root); que.size(); flag=!flag){
			int size = que.size();
			vector<int> v(size);
			for(int i=0; i<size; ++i, que.pop()){
				TreeNode *q = que.front();
				flag ? v[i]=q->val : v[size-i-1]=q->val;
				if(q->left) que.push(q->left);
				if(q->right) que.push(q->right);
			}
			vs.push_back(v);
		}
		return vs;
	}
};
