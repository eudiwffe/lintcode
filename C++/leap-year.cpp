/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/leap-year
   @Language: C++
   @Datetime: 19-04-19 16:59
   */

class Solution {
public:
	/**
	 * @param n: a number represent year
	 * @return: whether year n is a leap year.
	 */
	bool isLeapYear(int n) {
		// write your code here
		return (n%400==0 || (n%100 && n%4==0));
	}
};
