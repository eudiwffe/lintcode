/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/happy-number
   @Language: C++
   @Datetime: 16-02-09 04:48
   */

class Solution {
public:
	/**
	 * @param n an integer
	 * @return true if this is a happy number or false
	 */
	bool isHappy(int n) {
		// Write your code here
		unordered_set<int> hs;
		char s[32];
		for(int i; hs.emplace(n).second;){
			if (n==1) return true;
			sprintf(s,"%d",n);
			for(n=0,i=strlen(s); i--; n+=(s[i]-'0')*(s[i]-'0'));
		}
		return false;
	}
};
