/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/arranging-coins
   @Language: C++
   @Datetime: 19-05-08 15:55
   */

class Solution {
public:
	/**
	 * @param n: a non-negative integer
	 * @return: the total number of full staircase rows that can be formed
	 */
	int arrangeCoins(int n) {
		// Write your code here
		return (sqrt(1+8L*n)-1)/2;
	}
};
