/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-path-sum
   @Language: C++
   @Datetime: 17-03-31 17:41
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */
class Solution {
	void preorder(TreeNode *rt, int tar, vector<int> &vs
			,vector<vector<int> > &ans){
		if(rt==NULL) return;
		vs.push_back(rt->val);
		if(!rt->left && !rt->right && tar-rt->val==0)
			ans.push_back(vs);
		if(rt->left) preorder(rt->left,tar-rt->val,vs,ans);
		if(rt->right) preorder(rt->right,tar-rt->val,vs,ans);
		vs.pop_back();
	}
	
public:
	/**
	 * @param root the root of binary tree
	 * @param target an integer
	 * @return all valid paths
	 * Tip : Using Preorder traversal (DFS)
	 */
	vector<vector<int> > binaryTreePathSum(TreeNode *root, int target) {
		// Write your code here
		vector<vector<int> > ans;
		vector<int> vs;
		preorder(root,target,vs,ans);
		return ans;
	}
};
