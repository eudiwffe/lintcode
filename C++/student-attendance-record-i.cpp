/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/student-attendance-record-i
   @Language: C++
   @Datetime: 19-05-16 09:51
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: whether the student could be rewarded according to his attendance record
	 */
	bool checkRecord(string &s) {
		// Write your code here
		for(int i=0, a=0; i<s.length(); ++i){
			if(s[i]=='A') ++a;
			if(a>1) return false;
		}
		return s.find("LLL")==s.npos;
	}
};
