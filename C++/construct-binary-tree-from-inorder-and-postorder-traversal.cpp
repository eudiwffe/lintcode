/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/construct-binary-tree-from-inorder-and-postorder-traversal
   @Language: C++
   @Datetime: 16-02-09 05:45
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	/**
	 *@param inorder : A list of integers that inorder traversal of a tree
	 *@param postorder : A list of integers that postorder traversal of a tree
	 *@return : Root of a tree
	 */
	TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder
			,int begin,int end,int &pos){
		if (pos<0 || begin>=end) return NULL;
		TreeNode *root = new TreeNode(postorder[pos]);
		int cur = begin;
		for(; cur<end && inorder[cur]!=postorder[pos]; ++cur);
		--pos;
		root->right = buildTree(inorder,postorder,cur+1,end,pos);
		root->left = buildTree(inorder,postorder,begin,cur,pos);
		return root;
	}

public:
	TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
		// write your code here
		int pos = postorder.size()-1;
		return buildTree(inorder,postorder,0,inorder.size(),pos);
	}
};
