/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-size-subarray-sum
   @Language: C++
   @Datetime: 16-02-09 08:18
   */

class Solution {
public:
	/**
	 * @param nums: a vector of integers
	 * @param s: an integer
	 * @return: an integer representing the minimum size of subarray
	 */
	int minimumSize(vector<int> &nums, int s) {
		// write your code here
		int len=INT_MAX, sum=0, pos=-1;
		for(int i=0; i<nums.size(); ++i){
			sum += nums[i];
			while(sum>=s){
				len = (len<i-pos ? len:i-pos);
				sum -= nums[++pos];
			}
		}
		return len==INT_MAX ? -1:len;
	}
};
