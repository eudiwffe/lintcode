/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/permutations-ii
   @Language: C++
   @Datetime: 16-02-09 08:21
   */

class Solution {
public:
	/**
	 * @param nums: A list of integers.
	 * @return: A list of unique permutations.
	 */
	vector<vector<int> > permuteUnique(vector<int> &nums) {
		// write your code here
		vector<vector<int> > vs;
		if (nums.size()==0) return vs;
		sort(nums.begin(),nums.end());
		for(vs.push_back(nums);next_permutation(nums.begin(),nums.end());vs.push_back(nums));
		return vs;
	}
};
