/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/same-tree
   @Language: C++
   @Datetime: 16-02-09 04:49
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

// identical-binary-tree
class Solution {
public:
	/**
	 * @param a: the root of binary tree a.
	 * @param b: the root of binary tree b.
	 * @return: true if they are identical, or false.
	 */
	bool isIdentical(TreeNode * a, TreeNode * b) {
		// write your code here
		if (a==NULL && b==NULL) return true;
		if (a==NULL || b==NULL) return false;
		if (a->val!=b->val) return false;
		return isIdentical(a->left,b->left) && isIdentical(a->right,b->right);
	}
};
