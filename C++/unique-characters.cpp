/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/unique-characters
   @Language: C++
   @Datetime: 16-02-09 05:22
   */

class Solution {
public:
	/*
	 * @param str: A string
	 * @return: a boolean
	 */
	bool isUnique(string &str) {
		// write your code here
		bool letters[128]={false};
		for(const char &ch:str)
			if(letters[ch]) return false;
			else letters[ch]=true;
		return true;
	}
};
