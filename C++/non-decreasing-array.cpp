/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/non-decreasing-array
   @Language: C++
   @Datetime: 19-05-14 14:23
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @return: if it could become non-decreasing by modifying at most 1 element
	 */
	bool checkPossibility(vector<int> &nums) {
		// Write your code here
		for(int i=1; i<nums.size(); ++i){
			if(nums[i-1]>nums[i]){
				nums[i-1]=nums[i];
				break;
			}
		}
		for(int i=1; i<nums.size(); ++i)
			if(nums[i-1]>nums[i])
				return false;
		return true;
	}
};
