/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/maximum-average-subarray
   @Language: C++
   @Datetime: 19-04-28 17:11
   */

class Solution {
public:
	/**
	 * @param nums: an array
	 * @param k: an integer
	 * @return: the maximum average value
	 */
	double findMaxAverage(vector<int> &nums, int k) {
		// Write your code here
		int mx=INT_MIN, sum=0;
		for(int i=0; i<nums.size(); ++i){
			sum+=nums[i];
			if(i+1>=k){
				mx=max(mx,sum);
				sum-=nums[i-k+1];
			}
		}
		return 1.0*mx/k;
	}
};
