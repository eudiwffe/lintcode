/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/factorial-trailing-zeroes
   @Language: C++
   @Datetime: 19-05-27 13:49
   */

class Solution {
public:
	/**
	 * @param n: a integer
	 * @return: return a integer
	 */
	int trailingZeroes(int n) {
		// write your code here
		int count=0;
		for(; n; count+=(n/=5));
		return count;
	}
};
