/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/palindrome-number
   @Language: C++
   @Datetime: 19-03-31 08:20
   */

class Solution {
public:
	/**
	 * @param num: a positive number
	 * @return: true if it's a palindrome or false
	 */
	bool isPalindrome(int num) {
		// write your code here
		char str[32];
		sprintf(str,"%d",num);
		for(int i=0, j=strlen(str)-1; i<j; ++i,--j)
			if(str[i]!=str[j]) return false;
		return true;
	}
};
