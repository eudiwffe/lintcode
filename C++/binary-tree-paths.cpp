/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-paths
   @Language: C++
   @Datetime: 16-02-09 04:41
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	void preorder(TreeNode *rt,string str,vector<string> &ans){
		if(rt==NULL) return;
		str.append(to_string(rt->val));
		if(!rt->left && !rt->right)
			ans.push_back(str);
		if(rt->left) preorder(rt->left,str+"->",ans);
		if(rt->right) preorder(rt->right,str+"->",ans);
	}

public:
	/**
	 * @param root the root of the binary tree
	 * @return all root-to-leaf paths
	 * Tip: Pre-Order traversal (DFS)
	 */
	vector<string> binaryTreePaths(TreeNode* root) {
		// Write your code here
		vector<string> ans;
		preorder(root,"",ans);
		return ans;
	}
};
