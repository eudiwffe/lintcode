/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/word-pattern
   @Language: C++
   @Datetime: 19-04-27 14:13
   */


class Solution {
public:
	/**
	 * @param pattern: a string, denote pattern string
	 * @param teststr: a string, denote matching string
	 * @return: an boolean, denote whether the pattern string and the matching string match or not
	 */
	bool wordPattern(string &pattern, string &teststr) {
		// write your code here
		unordered_map<char,string> match;
		unordered_map<string,char> rever;
		stringstream istr(teststr);
		string str;
		for(const char &ch:pattern){
			if(!(istr>>str)) return false;
			if(rever.count(str)<1) rever[str]=ch;
			else if(rever[str]!=ch) return false;
			if(match.count(ch)<1) match[ch]=str;
			else if (match[ch]!=str) return false;
		}
		return true;
	}
};
