/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/maximum-product-of-three-numbers
   @Language: C++
   @Datetime: 19-05-14 15:46
   */

class Solution {
public:
	/**
	 * @param nums: an integer array
	 * @return: the maximum product
	 */
	int maximumProduct(vector<int> &nums) {
		// Write your code here
		sort(nums.begin(),nums.end());
		int n=nums.size();
		return max(nums[n-1]*nums[n-2]*nums[n-3],nums[0]*nums[1]*nums[n-1]);
	}
};
