/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/rotate-image
   @Language: C++
   @Datetime: 16-02-09 08:26
   */

class Solution {
public:
	/**
	 * @param matrix: A list of lists of integers
	 * @return: Void
	 */
	void rotate(vector<vector<int> > &M) {
		// write your code here
		int n = M.size(), t;
		if (n<2 || M[0].size()!=n) return;
		for (int i=0; i < n/2; ++i){
			for (int j=i; j< n-1-i; ++j){
				t = M[i][j];
				M[i][j] = M[n-j-1][i];
				M[n-j-1][i] = M[n-i-1][n-j-1];
				M[n-i-1][n-j-1] = M[j][n-i-1];
				M[j][n-i-1] = t;
			}
		}
	}
};
