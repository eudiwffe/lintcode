/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/string-to-integer-atoi
   @Language: C++
   @Datetime: 16-02-09 09:02
   */

class Solution {
public:
	/**
	 * @param str: A string
	 * @return: An integer
	 */
	int atoi(string &str) {
		// write your code here
		long num=0;
		sscanf(str.c_str(),"%ld",&num);
		if(num<INT_MAX && num>INT_MIN) return num;
		else return num>0?INT_MAX:INT_MIN;
	}
};
