/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-ascii-encoded-strings
   @Language: C++
   @Datetime: 19-03-13 17:33
   */

class Solution {
public:
	/**
	 * @param encodeString: an encode string
	 * @return: a reversed decoded string
	 */
	string reverseAsciiEncodedString(string &encodeString) {
		// Write your code here
		int len=encodeString.length();
		string s(len/2,'0');
		for(int i=0; i<s.length(); len-=2)
			s[i++]=(encodeString[len-2]-'0')*10+encodeString[len-1]-'0';
		return s;
	}
};
