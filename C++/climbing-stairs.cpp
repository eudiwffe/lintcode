/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/climbing-stairs
   @Language: C++
   @Datetime: 16-02-09 04:45
   */

class Solution {
public:
	/**
	 * @param n: An integer
	 * @return: An integer
	 * Tip: fibonacci
	 */
	int climbStairs(int n) {
		// write your code here
		if(n==0) return 0;
		int f0=0,fn=1;
		for(; n--; fn+=f0, f0=fn-f0);
		return fn;
	}
};
