/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/calculate-circumference-and-area
   @Language: C++
   @Datetime: 19-04-26 17:19
   */

class Solution {
public:
	/**
	 * @param r: a Integer represent radius
	 * @return: the circle's circumference nums[0] and area nums[1]
	 */
	vector<double> calculate(int r) {
		// write your code here
		return {2*3.14*r, 3.14*r*r};
	}
};
