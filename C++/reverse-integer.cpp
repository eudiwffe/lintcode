/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-integer
   @Language: C++
   @Datetime: 16-02-09 05:16
   */

class Solution {
public:
	/**
	 * @param n the integer to be reversed
	 * @return the reversed integer
	 */
	int reverseInteger(int n) {
		// Write your code here
		char tmp[32];
		sprintf(tmp,"%d",n);
		reverse(tmp+(tmp[0]=='-'?1:0),tmp+strlen(tmp));
		long r=atol(tmp);
		if(r>INT_MAX || r<INT_MIN) return 0;
		else return r;
	}
};
