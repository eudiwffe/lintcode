/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/diameter-of-binary-tree
   @Language: C++
   @Datetime: 19-05-16 10:25
   */


/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	int postorder(TreeNode *root, int &maxd){
		if(root==NULL) return 0;
		int left=postorder(root->left,maxd);
		int right=postorder(root->right,maxd);
		maxd=max(maxd,left+right+1);
		return max(left,right)+1;
	}
public:
	/**
	 * @param root: a root of binary tree
	 * @return: return a integer
	 */
	int diameterOfBinaryTree(TreeNode * root) {
		// write your code here
		int maxd=0;
		postorder(root,maxd);
		return maxd-1;
	}
};
