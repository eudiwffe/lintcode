/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/fibonacci
   @Language: C++
   @Datetime: 16-02-09 04:39
   */

class Solution {
public:
	/**
	 * @param n: an integer
	 * @return: an ineger f(n)
	 */
	int fibonacci(int n) {
		// write your code here
		int f0=1, fn=0;
		for(; --n; fn+=f0, f0=fn-f0);
		return fn;
	}
};
