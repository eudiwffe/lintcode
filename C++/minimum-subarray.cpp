/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/minimum-subarray
   @Language: C++
   @Datetime: 16-02-09 05:12
   */

class Solution {
public:
	/**
	 * @param nums: a list of integers
	 * @return: A integer denote the sum of minimum subarray
	 */
	int minSubArray(vector<int> nums) {
		// write your code here
		int sum=0, mx=INT_MAX;
		for(int i=0; i<nums.size(); ++i){
			sum += nums[i];
			mx = min(mx,sum);
			if (sum>0) sum=0;
		}
		return mx;
	}
};
