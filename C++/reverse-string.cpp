/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-string
   @Language: C++
   @Datetime: 19-05-23 13:21
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: return a string
	 */
	string reverseString(string &s) {
		// write your code here
		reverse(s.begin(),s.end());
		for(int i=0; (i=s.find('\\',i))!=s.npos; swap(s[i-1],s[i]));
		return s;
	}
};
