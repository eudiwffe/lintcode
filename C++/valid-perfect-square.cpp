/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/valid-perfect-square
   @Language: C++
   @Datetime: 19-04-26 17:45
   */

class Solution {
public:
	/**
	 * @param num: a positive integer
	 * @return: if num is a perfect square else False
	 */
	bool isPerfectSquare(int num) {
		// write your code here
		return pow((int)sqrt(num),2)==num;
	}
};
