/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-elements-in-matrix
   @Language: C++
   @Datetime: 19-04-25 15:49
   */

class Solution {
public:
	/**
	 * @param Matrix: the input
	 * @return: the element which appears every row
	 */
	int FindElements(vector<vector<int> > &matrix) {
		// write your code here
		unordered_map<int,int> dict;
		for(const auto &row:matrix){
			unordered_set<int> rows;
			for(const auto &e:row){
				if(rows.count(e)) continue;
				rows.insert(e);
				dict[e]++;
			}
		}
		for(const auto &p:dict)
			if(p.second==matrix.size()) return p.first;
		return 0;
	}
};
