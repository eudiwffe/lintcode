/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-preorder-traversal
   @Language: C++
   @Datetime: 16-02-09 04:45
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

// Method 1, recursion, Time 10ms
class Solution {
	void preorder(TreeNode *r, vector<int> &vs){
		if (r==NULL)
			return ;
		vs.push_back(r->val);
		preorder(r->left,vs);
		preorder(r->right,vs);
	}

public:
	/**
	 * @param root: The root of binary tree.
	 * @return: Preorder in vector which contains node values.
	 */
	vector<int> preorderTraversal(TreeNode *root) {
		// write your code here
		vector<int> vs;
		preorder(root,vs);
		return vs;
	}
};

// Method 2, loop with stack, Time 6ms
class Solution {
public:
	/**
	 * @param root: A Tree
	 * @return: Preorder in ArrayList which contains node values.
	 */
	vector<int> preorderTraversal(TreeNode * root) {
		// write your code here
		if(root==NULL) return {};
		vector<int> v;
		stack<TreeNode*> s;
		for(s.push(root); s.size();){
			TreeNode *node=s.top();
			s.pop();
			v.push_back(node->val);
			if(node->right) s.push(node->right);
			if(node->left) s.push(node->left);
		}
		return v;
	}
};
