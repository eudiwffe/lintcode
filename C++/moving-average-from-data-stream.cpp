/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/moving-average-from-data-stream
   @Language: C++
   @Datetime: 19-03-17 18:51
   */

class MovingAverage {
	long long sum;
	int size;
	queue<int> vals;
public:
	/*
	 * @param size: An integer
	 */MovingAverage(int size) {
		 // do intialization if necessary
		 sum=0;
		 this->size=size;
		 for(; vals.size()>0; vals.pop());
	 }

	 /*
	  * @param val: An integer
	  * @return:  
	  */
	 double next(int val) {
		 // write your code here
		 if (vals.size()==size){
			 sum -= vals.front();
			 vals.pop();
		 }
		 sum += val;
		 vals.push(val);
		 return sum*1.0/vals.size();
	 }
};

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param = obj.next(val);
 */
