/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/length-of-last-word
   @Language: C++
   @Datetime: 16-02-09 04:50
   */

class Solution {
public:
	/**
	 * @param s: A string
	 * @return: the length of last word
	 * Tip:
	 * "a ", "a b "
	 */
	int lengthOfLastWord(string &s) {
		// write your code here
		size_t end=s.find_last_not_of(' ');
		if(end>s.length()) return 0;
		size_t start=s.rfind(' ',end);
		if(start>s.length()) return end+1;
		return end-start;
	}
};
