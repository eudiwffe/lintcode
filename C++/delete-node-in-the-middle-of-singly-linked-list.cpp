/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/delete-node-in-the-middle-of-singly-linked-list
   @Language: C++
   @Datetime: 16-02-09 04:47
   */

/**
 * Definition of ListNode
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *         this->val = val;
 *         this->next = NULL;
 *     }
 * }
 */
class Solution {
public:
	/**
	 * @param node: a node in the list should be deleted
	 * @return: nothing
	 * Tip; to be deleted node is Not head or tail
	 */
	void deleteNode(ListNode *node) {
		// write your code here
		ListNode *p = node->next;
		node->val = p->val;
		node->next = p->next;
		delete p;
	}
};
