/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/subsets-ii
   @Language: C++
   @Datetime: 17-03-13 15:38
   */

class Solution {
public:
	/**
	 * @param S: A set of numbers.
	 * @return: A list of lists. All valid subsets.
	 * Tip : See problem 18 subsets
	 */
	vector<vector<int> > subsetsWithDup(const vector<int> &S) {
		// write your code here
		vector<int> A(S);
		sort(A.begin(),A.end());
		vector<vector<int> > vs(1);
		for (int i=0,count=0; i<A.size(); ++i){
			int size = vs.size();
			for (int j=0; j<size; ++j){
				if (i==0 || A[i-1]!=A[i] || j>=count){
					vs.push_back(vs[j]);
					vs.back().push_back(A[i]);
				}
			}
			count=size;
		}
		return vs;
	}
};
