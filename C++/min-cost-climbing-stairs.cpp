/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/min-cost-climbing-stairs
   @Language: C++
   @Datetime: 19-05-13 14:17
   */

class Solution {
public:
	/**
	 * @param cost: an array
	 * @return: minimum cost to reach the top of the floor
	 */
	int minCostClimbingStairs(vector<int> &cost) {
		// Write your code here
		int n=cost.size();
		vector<int> dp(n+1,0);
		for(int i=2; i<=n; ++i)
			dp[i]=min(dp[i-1]+cost[i-1],dp[i-2]+cost[i-2]);
		return dp[n];
	}
};

