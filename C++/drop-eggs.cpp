/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/drop-eggs
   @Language: C++
   @Datetime: 19-03-14 13:28
   */

class Solution {
public:
	/**
	 * @param n: An integer
	 * @return: The sum of a and b
	 * Tip:
	 *    x+(x-1)+(x-2)+...+1 >= 100
	 */
	int dropEggs(int n) {
		// write your code here
		double x=(sqrt(8.0*n+1.0)-1)/2;
		return x>(int)x?x+1:x;
	}
};
