/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/number-of-segments-in-a-string
   @Language: C++
   @Datetime: 19-05-22 16:41
   */

// Method 1, stringstream, Time 50ms
class Solution {
public:
	/**
	 * @param s: a string
	 * @return: the number of segments in a string
	 */
	int countSegments(string &s) {
		// write yout code here
		int count=0;
		stringstream istr(s);
		for(string str; istr>>str; ++count);
		return count;
	}
};

// Method 2, travelsal, Time 6ms
class Solution {
public:
	/**
	 * @param s: a string
	 * @return: the number of segments in a string
	 */
	int countSegments(string &s) {
		// write yout code here
		int count=0;
		s.push_back(' ');
		for(int i=1; i<s.length(); ++i)
			if(s[i-1]!=' ' && s[i]==' ') ++count;
		return count;
	}
};
