/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/intersection-of-two-arrays
   @Language: C++
   @Datetime: 16-12-07 08:09
   */

class Solution {
public:
	/**
	 * @param nums1 an integer array
	 * @param nums2 an integer array
	 * @return an integer array
	 * Tip: intersection array's elements are unique.
	 */
	vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
		// Write your code here
		unordered_set<int> h1(nums1.begin(),nums1.end());
		vector<int> v;
		for(int i=nums2.size(); i--; h1.erase(nums2[i]))
			if (h1.find(nums2[i])!=h1.end())
				v.push_back(nums2[i]);
		return v;
	}
};
