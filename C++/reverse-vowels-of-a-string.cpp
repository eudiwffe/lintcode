/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/reverse-vowels-of-a-string
   @Language: C++
   @Datetime: 19-03-25 13:44
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @return: reverse only the vowels of a string
	 */
	string reverseVowels(string &s) {
		// write your code here
		unordered_set<char> vowels={'a','e','i','o','u','A','E','I','O','U'};
		for(int i=0, j=s.length()-1; i<j;++i,--j){
			for(;!vowels.count(s[i]);++i);
			for(;!vowels.count(s[j]);--j);
			if(i<j) swap(s[i],s[j]);
		}
		return s;
	}
};
