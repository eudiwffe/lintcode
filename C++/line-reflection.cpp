/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/line-reflection
   @Language: C++
   @Datetime: 19-05-22 16:13
   */

class Solution {
	struct hashvi{
		size_t operator()(const vector<int> &v)const{
			return (v[0]<<16)|v[1];
		}
	};
public:
	/**
	 * @param points: n points on a 2D plane
	 * @return: if there is such a line parallel to y-axis that reflect the given points
	 */
	bool isReflected(vector<vector<int> > &points) {
		// Write your code here
		int left=INT_MAX, right=INT_MIN;
		unordered_set<vector<int>,hashvi> ps;
		for(const auto &point:points){
			left=min(left,point[0]);
			right=max(right,point[0]);
			ps.insert(point);
		}
		for(const auto &point:points){
			vector<int> p={left+right-point[0],point[1]};
			if(ps.count(p)<1) return false;
		}
		return true;
	}
};
