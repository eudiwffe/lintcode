/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-the-missing-number
   @Language: C++
   @Datetime: 16-02-09 05:49
   */

class Solution {
public:
	/**    
	 * @param nums: a vector of integers
	 * @return: an integer
	 * Tip: range [0,N], number of nums is N-1,
	 *      means only miss one num.
	 */
	int findMissing(vector<int> &nums) {
		// write your code here
		int r=0, i=0;
		for(; i<nums.size(); ++i)
			r ^= nums[i]^(i+1);
		return r;
	}
};
