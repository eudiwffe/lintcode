/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/find-the-difference
   @Language: C++
   @Datetime: 19-05-23 13:06
   */

class Solution {
public:
	/**
	 * @param s: a string
	 * @param t: a string
	 * @return: the letter that was added in t
	 */
	char findTheDifference(string &s, string &t) {
		// Write your code here
		char ch=0;
		for(int i=s.length(); i--; ch^=s[i]);
		for(int i=t.length(); i--; ch^=t[i]);
		return ch;
	}
};
