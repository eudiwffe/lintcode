/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/construct-the-rectangle
   @Language: C++
   @Datetime: 19-05-16 15:22
   */

class Solution {
public:
	/**
	 * @param area: web pageâ€™s area
	 * @return: the length L and the width W of the web page you designed in sequence
	 */
	vector<int> constructRectangle(int area) {
		// Write your code here
		for(int l=sqrt(area); l<=area; ++l)
			if(area%l==0) return {l,area/l};
		return {area,1};
	}
};
