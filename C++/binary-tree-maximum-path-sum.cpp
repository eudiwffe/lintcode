/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-maximum-path-sum
   @Language: C++
   @Datetime: 19-03-29 11:43
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
	int maxpath;
	int getpath(TreeNode *root){
		if (root==NULL) return 0;
		int left=max(0,getpath(root->left));
		int right=max(0,getpath(root->right));
		maxpath = max(maxpath, root->val+left+right);
		return root->val+max(left,right);
	}
public:
	/**
	 * @param root: The root of binary tree.
	 * @return: An integer
	 * Tip:
	 * each node, not only leaf
	 */
	int maxPathSum(TreeNode * root) {
		// write your code here
		maxpath=INT_MIN;
		getpath(root);
		return maxpath;
	}
};
