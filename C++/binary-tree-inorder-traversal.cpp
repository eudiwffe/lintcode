/*
   @Copyright:LintCode
   @Author:   tjyemail
   @Problem:  http://www.lintcode.com/problem/binary-tree-inorder-traversal
   @Language: C++
   @Datetime: 16-02-09 04:41
   */

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

// Method 1, recursion, Time 7ms
class Solution {
	/**
	 * @param root: The root of binary tree.
	 * @return: Inorder in vector which contains node values.
	 */
	void inorder(TreeNode *r,vector<int> &vs){
		if (r==NULL) return;
		inorder(r->left,vs);
		vs.push_back(r->val);
		inorder(r->right,vs);
	}

public:
	vector<int> inorderTraversal(TreeNode *root) {
		// write your code here
		vector<int> vs;
		inorder(root,vs);
		return vs;
	}
};

// Method 2, loop with stack, Time 50ms
class Solution {
public:
	/**
	 * @param root: A Tree
	 * @return: Inorder in ArrayList which contains node values.
	 */
	vector<int> inorderTraversal(TreeNode * root) {
		// write your code here
		if(root==NULL) return {};
		vector<int> v;
		stack<TreeNode*> s;
		for(TreeNode *node=root; node; node=node->left)
			s.push(node);
		for(; s.size();){
			TreeNode *node=s.top();
			s.pop();
			v.push_back(node->val);
			for(node=node->right; node; node=node->left)
				s.push(node);
		}
		return v;
	}
};
